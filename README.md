# Bitbucket Cloud CLI

## Install

Clone or download the repository, and run `npm install -g` on the repository root directory.

## Configure

Please put .bbrc file.

An example of .bbrc:

```json
{
  "auth": {
    "username": "bitbucket-user"
  }
}
```

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).
