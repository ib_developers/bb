import { Command } from 'commander';
import { Readable, Writable } from 'stream';

export type Scm = 'hg' | 'git';
export type ForkPolicy = 'allow_forks' | 'no_public_forks' | 'no_forks';

export type SubCommand = (inStream: Readable, outStream: Writable) => Command;
