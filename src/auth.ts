import prompts from 'prompts';
import { AuthOptions } from 'request';
import { pickBy, isEmpty } from 'lodash';
import conf from './conf';

const auth = async () => {
  if (process.env.NODE_ENV === 'test') {
    if (!conf.test) {
      throw new Error();
    }

    return conf.test.auth;
  }

  let authOptions: AuthOptions = { ...conf.auth };

  const response = await prompts([
    {
      type: !authOptions.username && 'text',
      name: 'username',
      message: 'Username for Bitbucket Cloud'
    },
    {
      type: (_, { username }) =>
        (username || authOptions.username) && !authOptions.password && 'password',
      name: 'password',
      message: (_, { username }) => `Password for ${username || authOptions.username}`
    }
  ]);

  authOptions = { ...authOptions, ...pickBy(response) };

  if (!authOptions.password) {
    delete authOptions.username;
  }

  return isEmpty(authOptions) ? undefined : authOptions;
};

export default auth;
