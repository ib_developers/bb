import {
  Account,
  AccountAllOfLinks,
  Activity,
  Branch,
  BranchingModelSettingsAllOfLinksSelf,
  Comment,
  CommentAllOfLinks,
  CommentContent,
  CommentInline,
  Issue,
  IssueAllOfContent,
  IssueAllOfLinks,
  Pullrequest,
  PullrequestAllOfLinks,
  PullrequestEndpoint,
  PullrequestEndpointBranch,
  PullrequestEndpointCommit,
  Repository,
  RepositoryAllOfLinks,
  User
} from './api/model/models';

interface Parser {
  map: { name: string; baseName: string; type: string }[];
  default: string[];
}

const parsers: { [key: string]: Parser | undefined } = {
  Account: {
    map: Account.getAttributeTypeMap(),
    default: ['display_name', 'website', 'has_2fa_enabled', 'account_status']
  },
  AccountAllOfLinks: {
    map: AccountAllOfLinks.getAttributeTypeMap(),
    default: []
  },
  Activity: {
    map: Activity.getAttributeTypeMap().concat([
      {
        name: 'approval',
        baseName: 'approval',
        type: 'Approval'
      },
      {
        name: 'pull_request',
        baseName: 'pull_request',
        type: 'Pullrequest'
      },
      {
        name: 'update',
        baseName: 'update',
        type: 'Update'
      }
    ]),
    default: ['approval', 'comment', 'pull_request', 'update']
  },
  Approval: {
    map: [
      {
        name: 'date',
        baseName: 'date',
        type: 'Date'
      },
      {
        name: 'pullrequest',
        baseName: 'pullrequest',
        type: 'Pullrequest'
      },
      {
        name: 'user',
        baseName: 'user',
        type: 'User'
      }
    ],
    default: ['date', 'pullrequest', 'user']
  },
  Branch: {
    map: Branch.getAttributeTypeMap(),
    default: ['name', 'target']
  },
  BranchingModelSettingsAllOfLinksSelf: {
    map: BranchingModelSettingsAllOfLinksSelf.getAttributeTypeMap(),
    default: []
  },
  Comment: {
    map: Comment.getAttributeTypeMap(),
    default: ['id', 'created_on', 'updated_on', 'deleted', 'user', 'inline', 'parent', 'content']
  },
  CommentAllOfLinks: {
    map: CommentAllOfLinks.getAttributeTypeMap(),
    default: []
  },
  CommentContent: {
    map: CommentContent.getAttributeTypeMap(),
    default: ['raw']
  },
  CommentInline: {
    map: CommentInline.getAttributeTypeMap(),
    default: ['path', 'from', 'to']
  },
  Issue: {
    map: Issue.getAttributeTypeMap(),
    default: ['priority', 'kind', 'reporter', 'created_on', 'updated_on', 'content']
  },
  IssueAllOfLinks: {
    map: IssueAllOfLinks.getAttributeTypeMap(),
    default: []
  },
  IssueAllOfContent: {
    map: IssueAllOfContent.getAttributeTypeMap(),
    default: ['raw']
  },
  Pullrequest: {
    map: Pullrequest.getAttributeTypeMap(),
    default: [
      'id',
      'title',
      'state',
      'source',
      'destination',
      'author',
      'comment_count',
      'task_count',
      'created_on',
      'updated_on',
      'summary'
    ]
  },
  PullrequestAllOfLinks: {
    map: PullrequestAllOfLinks.getAttributeTypeMap(),
    default: []
  },
  PullrequestEndpoint: {
    map: PullrequestEndpoint.getAttributeTypeMap(),
    default: ['repository', 'commit', 'branch']
  },
  PullrequestEndpointBranch: {
    map: PullrequestEndpointBranch.getAttributeTypeMap(),
    default: ['default_merge_strategy', 'name', 'merge_strategies']
  },
  PullrequestEndpointCommit: {
    map: PullrequestEndpointCommit.getAttributeTypeMap(),
    default: ['commit']
  },
  Repository: {
    map: Repository.getAttributeTypeMap(),
    default: ['is_private', 'fork_policy', 'scm', 'language', 'description']
  },
  RepositoryAllOfLinks: {
    map: RepositoryAllOfLinks.getAttributeTypeMap(),
    default: []
  },
  Update: {
    map: [
      {
        name: 'description',
        baseName: 'description',
        type: 'string'
      },
      {
        name: 'title',
        baseName: 'title',
        type: 'string'
      },
      {
        name: 'destination',
        baseName: 'destination',
        type: 'PullrequestEndpoint'
      },
      {
        name: 'reason',
        baseName: 'reason',
        type: 'string'
      },
      {
        name: 'source',
        baseName: 'source',
        type: 'PullrequestEndpoint'
      },
      {
        name: 'state',
        baseName: 'state',
        type: 'Pullrequest.StateEnum'
      },
      {
        name: 'author',
        baseName: 'author',
        type: 'Account'
      },
      {
        name: 'date',
        baseName: 'date',
        type: 'Date'
      }
    ],
    default: ['date', 'title', 'description', 'author', 'source', 'destination', 'state', 'reason']
  },
  User: {
    map: User.getAttributeTypeMap(),
    default: ['display_name', 'website', 'has_2fa_enabled', 'account_status', 'is_staff']
  }
};

export default parsers;
