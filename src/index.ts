#!/usr/bin/env node
import program from 'commander';
import { version } from '../package.json';

program.version(version);

program.command(
  'activities',
  'https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/pullrequests/activity'
);
program.command(
  'issue',
  'https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/issues/%7Bissue_id%7D'
);
program.command(
  'issues',
  'https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/issues'
);
program.command(
  'pullrequests',
  'https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/pullrequests'
);
program.command(
  'repositories',
  'https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories'
);
program.command(
  'repository',
  'https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D'
);

program.parse(process.argv);
