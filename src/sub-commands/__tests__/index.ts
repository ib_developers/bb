import colors from 'colors';
import { Readable, Writable } from 'stream';
import { WritableStream } from 'memory-streams';
import conf from '../../conf';
import activities from '../activities';
import repository from '../repository';
import repositories from '../repositories';
import issue from '../issue';
import issues from '../issues';
import pullrequests from '../pullrequests';

const now = new Date('2019/10/24 12:00:00');
Date.now = jest.fn(() => now.valueOf());

process.exit = jest.fn((code?: number) => {
  throw new Error(`Process exited with status ${code}`);
});

colors.enable();

test('should put a repository', async () => {
  const inStream = new Readable();
  const outStream = new Writable();

  await new Promise(resolve => {
    if (!conf.test || !conf.test.username || !conf.test.repoSlug) {
      throw new Error();
    }

    repository(inStream, outStream)
      .parse([
        'node',
        'bb-repository',
        '--put',
        'src/sub-commands/__tests__/put-repository.json',
        conf.test.username,
        conf.test.repoSlug
      ])
      .on('end', resolve);
  });
});

describe('activities sub command', () => {
  it('should list activities', async () => {
    const inStream = new Readable();
    const outStream = new WritableStream();

    await new Promise(resolve => {
      activities(inStream, outStream)
        .parse(['node', 'bb-activities', 'hata6502', 'test', '1'])
        .on('end', resolve);
    });

    expect(outStream.toString()).toMatchSnapshot();
  });

  it('should get activities', async () => {
    const inStream = new Readable();
    const outStream = new WritableStream();

    await new Promise(resolve => {
      activities(inStream, outStream)
        .parse(['node', 'bb-activities', 'hata6502', 'test', '1', '--get'])
        .on('end', resolve);
    });

    expect(outStream.toString()).toMatchSnapshot();
  });
});

describe('issue sub command', () => {
  it('should get a issue', async () => {
    const inStream = new Readable();
    const outStream = new WritableStream();

    await new Promise(resolve => {
      issue(inStream, outStream)
        .parse(['node', 'bb-issue', 'hata6502', 'test', '1'])
        .on('end', resolve);
    });

    expect(outStream.toString()).toMatchSnapshot();
  });

  it('should post a issue', async () => {
    const inStream = new Readable();
    const outStream = new Writable();

    await new Promise(resolve => {
      if (!conf.test || !conf.test.username || !conf.test.repoSlug) {
        throw new Error();
      }

      issue(inStream, outStream)
        .parse([
          'node',
          'bb-issue',
          '--post',
          'src/sub-commands/__tests__/post-issue.json',
          conf.test.username,
          conf.test.repoSlug
        ])
        .on('end', resolve);
    });
  });

  it('should put a issue', async () => {
    const inStream = new Readable();
    const outStream = new Writable();

    await new Promise(resolve => {
      if (!conf.test || !conf.test.username || !conf.test.repoSlug) {
        throw new Error();
      }

      issue(inStream, outStream)
        .parse([
          'node',
          'bb-issue',
          '--put',
          'src/sub-commands/__tests__/put-issue.json',
          conf.test.username,
          conf.test.repoSlug,
          '1'
        ])
        .on('end', resolve);
    });
  });

  it('should delete a issue', async () => {
    const inStream = new Readable();
    const outStream = new Writable();

    await new Promise(resolve => {
      if (!conf.test || !conf.test.username || !conf.test.repoSlug) {
        throw new Error();
      }

      issue(inStream, outStream)
        .parse(['node', 'bb-issue', '--delete', conf.test.username, conf.test.repoSlug, '1'])
        .on('end', resolve);
    });
  });
});

describe('issues sub command', () => {
  it('should list issues', async () => {
    const inStream = new Readable();
    const outStream = new WritableStream();

    await new Promise(resolve => {
      issues(inStream, outStream)
        .parse(['node', 'bb-issues', 'hata6502', 'test'])
        .on('end', resolve);
    });

    expect(outStream.toString()).toMatchSnapshot();
  });

  it('should get issues', async () => {
    const inStream = new Readable();
    const outStream = new WritableStream();

    await new Promise(resolve => {
      issues(inStream, outStream)
        .parse(['node', 'bb-issues', 'hata6502', 'test', '--get'])
        .on('end', resolve);
    });

    expect(outStream.toString()).toMatchSnapshot();
  });
});

describe('pullrequests sub command', () => {
  it('should list pullrequests', async () => {
    const inStream = new Readable();
    const outStream = new WritableStream();

    await new Promise(resolve => {
      pullrequests(inStream, outStream)
        .parse(['node', 'bb-pullrequests', 'hata6502', 'test'])
        .on('end', resolve);
    });

    expect(outStream.toString()).toMatchSnapshot();
  });

  it('should get pullrequests', async () => {
    const inStream = new Readable();
    const outStream = new WritableStream();

    await new Promise(resolve => {
      pullrequests(inStream, outStream)
        .parse(['node', 'bb-pullrequests', 'hata6502', 'test', '--get'])
        .on('end', resolve);
    });

    expect(outStream.toString()).toMatchSnapshot();
  });
});

describe('repository sub command', () => {
  it('should get a repository', async () => {
    const inStream = new Readable();
    const outStream = new WritableStream();

    await new Promise(resolve => {
      repository(inStream, outStream)
        .parse(['node', 'bb-repository', 'hata6502', 'test'])
        .on('end', resolve);
    });

    expect(outStream.toString()).toMatchSnapshot();
  });
});

describe('repositories sub command', () => {
  it('should list repositories', async () => {
    const inStream = new Readable();
    const outStream = new WritableStream();

    await new Promise(resolve => {
      repositories(inStream, outStream)
        .parse(['node', 'bb-repositories', '-q', 'name="test"', 'hata6502'])
        .on('end', resolve);
    });

    expect(outStream.toString()).toMatchSnapshot();
  });

  it('should get repositories', async () => {
    const inStream = new Readable();
    const outStream = new WritableStream();

    await new Promise(resolve => {
      repositories(inStream, outStream)
        .parse(['node', 'bb-repositories', '-q', 'name="test"', 'hata6502', '--get'])
        .on('end', resolve);
    });

    expect(outStream.toString()).toMatchSnapshot();
  });
});

test('should delete a repository', async () => {
  const inStream = new Readable();
  const outStream = new Writable();

  await new Promise(resolve => {
    if (!conf.test || !conf.test.username || !conf.test.repoSlug) {
      throw new Error();
    }

    repository(inStream, outStream)
      .parse(['node', 'bb-repository', '--delete', conf.test.username, conf.test.repoSlug])
      .on('end', resolve);
  });
});
