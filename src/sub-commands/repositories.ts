#!/usr/bin/env node
import { Command } from 'commander';
import colors from 'colors/safe';
import yaml from 'js-yaml';
import { pickBy } from 'lodash';
import url from 'url';
import rp from 'request-promise';
import auth from '../auth';
import withCatch from '../withCatch';
import { parse, withMember } from '../parse';
import { SubCommand } from '../types';
import { PaginatedRepositories } from '../api/model/models';
import get from '../get';

const repositories: SubCommand = (_inStream, outStream) => {
  const program = withMember(
    new Command()
      .name('bb repositories')
      .arguments('[username]')
      .option('--get [file]')
      .option('-a, --after <date>')
      .option('-q, --query <query>')
      .option('-s, --sort <sort>')
  );

  program.action(async (username?: string) => {
    const response: PaginatedRepositories = await withCatch(
      rp({
        uri: url.format({
          protocol: 'https',
          hostname: 'api.bitbucket.org',
          pathname: `2.0/repositories${username ? `/${username}` : ''}`,
          query: pickBy({
            after: program.after,
            q: program.query,
            sort: program.sort
          })
        }),
        json: true,
        auth: await auth()
      })
    );

    if (program.get) {
      get(program.get, response, outStream);

      program.emit('end');
      return;
    }

    if (response.values) {
      response.values.forEach(repository => {
        outStream.write(`${colors.yellow(repository.full_name || '')}\n`);
        outStream.write(yaml.dump(parse(repository, 'Repository', program.member)));
        outStream.write('\n');
      });
    }

    if (response.next) {
      const next = url.parse(response.next, true);
      outStream.write(`To get next, run with "-a ${next.query.after}". \n`);
    }

    program.emit('end');
  });

  return program;
};

if (process.env.NODE_ENV !== 'test') {
  repositories(process.stdin, process.stdout).parse(process.argv);
}

export default repositories;
