#!/usr/bin/env node
import { Command } from 'commander';
import colors from 'colors/safe';
import yaml from 'js-yaml';
import { pickBy } from 'lodash';
import url from 'url';
import rp from 'request-promise';
import auth from '../auth';
import withCatch from '../withCatch';
import { parse, withMember } from '../parse';
import { SubCommand } from '../types';
import { PaginatedPullrequests } from '../api/model/models';
import get from '../get';

const pullrequests: SubCommand = (_inStream, outStream) => {
  const program = withMember(
    new Command()
      .name('bb pullrequests')
      .arguments('<username> <repo_slug>')
      .option('--get [file]')
      .option('-p, --page <page>')
      .option('-q, --query <query>')
      .option('-s, --sort <sort>')
  );

  program.action(async (username: string, repoSlug: string) => {
    const response: PaginatedPullrequests = await withCatch(
      rp({
        uri: url.format({
          protocol: 'https',
          hostname: 'api.bitbucket.org',
          pathname: `2.0/repositories/${username}/${repoSlug}/pullrequests`,
          query: pickBy({
            page: program.page,
            q: program.query,
            sort: program.sort
          })
        }),
        json: true,
        auth: await auth()
      })
    );

    if (program.get) {
      get(program.get, response, outStream);

      program.emit('end');
      return;
    }

    if (response.values) {
      response.values.forEach(pullrequest => {
        outStream.write(colors.yellow(`#${pullrequest.id || ''} ${pullrequest.title || ''}\n`));
        outStream.write(
          yaml.dump(parse(pullrequest, 'Pullrequest', program.member), program.member)
        );
        outStream.write('\n');
      });
    }

    if (response.next) {
      const next = url.parse(response.next, true);
      outStream.write(`To get next, run with "-p ${next.query.page}". \n`);
    }

    program.emit('end');
  });

  return program;
};

if (process.env.NODE_ENV !== 'test') {
  pullrequests(process.stdin, process.stdout).parse(process.argv);
}

export default pullrequests;
