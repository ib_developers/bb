#!/usr/bin/env node
import { Command } from 'commander';
import url from 'url';
import { readFileSync } from 'fs';
import { Options } from 'request';
import rp from 'request-promise';
import yaml from 'js-yaml';
import { parse, withMember } from '../parse';
import auth from '../auth';
import withCatch from '../withCatch';
import { SubCommand } from '../types';
import get from '../get';

const issue: SubCommand = (_inStream, outStream) => {
  const program = withMember(
    new Command()
      .name('bb issue')
      .arguments('<username> <repo_slug> [issue_id]')
      .option('--get [file]')
      .option('--post <file>')
      .option('--put <file>')
      .option('--delete')
  );

  program.action(async (username: string, repoSlug: string, issueId?: string) => {
    let request: Options = {
      uri: url.format({
        protocol: 'https',
        hostname: 'api.bitbucket.org',
        pathname: `/2.0/repositories/${username}/${repoSlug}/issues${issueId ? `/${issueId}` : ''}`
      }),
      json: true,
      auth: await auth()
    };

    if (program.get) {
      get(program.get, await withCatch(rp(request)), outStream);

      program.emit('end');
      return;
    }

    if (program.post) {
      const json = JSON.parse(readFileSync(program.post, { encoding: 'utf-8' }));
      request = { ...request, method: 'POST', json };

      await withCatch(rp(request));

      program.emit('end');
      return;
    }

    if (program.put) {
      const json = JSON.parse(readFileSync(program.put, { encoding: 'utf-8' }));
      request = { ...request, method: 'PUT', json };

      await withCatch(rp(request));

      program.emit('end');
      return;
    }

    if (program.delete) {
      request = { ...request, method: 'DELETE' };

      await withCatch(rp(request));

      program.emit('end');
      return;
    }

    outStream.write(
      yaml.dump(parse(await withCatch(rp(request)), 'Issue', program.member || null))
    );

    program.emit('end');
  });

  return program;
};

if (process.env.NODE_ENV !== 'test') {
  issue(process.stdin, process.stdout).parse(process.argv);
}

export default issue;
