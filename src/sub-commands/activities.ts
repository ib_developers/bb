#!/usr/bin/env node
import { Command } from 'commander';
import yaml from 'js-yaml';
import { pickBy } from 'lodash';
import url from 'url';
import rp from 'request-promise';
import auth from '../auth';
import withCatch from '../withCatch';
import { parse, withMember } from '../parse';
import { SubCommand } from '../types';
import { PaginatedActivities } from '../api/model/models';
import get from '../get';

const activities: SubCommand = (_inStream, outStream) => {
  const program = withMember(
    new Command()
      .name('bb activities')
      .arguments('<username> <repo_slug> [pull_request_id]')
      .option('--get [file]')
      .option('-c, --ctx <ctx>')
      .option('-q, --query <query>')
      .option('-s, --sort <sort>')
  );

  program.action(async (username: string, repoSlug: string, pullRequestId?: string) => {
    const response: PaginatedActivities = await withCatch(
      rp({
        uri: url.format({
          protocol: 'https',
          hostname: 'api.bitbucket.org',
          pathname: `2.0/repositories/${username}/${repoSlug}/pullrequests/${
            pullRequestId ? `${pullRequestId}/` : ''
          }activity`,
          query: pickBy({
            ctx: program.ctx,
            q: program.query,
            sort: program.sort
          })
        }),
        json: true,
        auth: await auth()
      })
    );

    if (program.get) {
      get(program.get, response, outStream);

      program.emit('end');
      return;
    }

    if (response.values) {
      response.values.forEach(activity => {
        outStream.write(yaml.dump(parse(activity, 'Activity', program.member)));
        outStream.write('\n');
      });
    }

    if (response.next) {
      const next = url.parse(response.next, true);
      outStream.write(`To get next, run with "-c ${next.query.ctx}". \n`);
    }

    program.emit('end');
  });

  return program;
};

if (process.env.NODE_ENV !== 'test') {
  activities(process.stdin, process.stdout).parse(process.argv);
}

export default activities;
