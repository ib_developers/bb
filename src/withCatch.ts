import { RequestPromise } from 'request-promise';
import { StatusCodeError } from 'request-promise/errors';
import HttpStatus from 'http-status-codes';
import yaml from 'js-yaml';
import { Error } from './api/model/models';

const withCatch = (rp: RequestPromise) =>
  rp.catch(StatusCodeError, ({ statusCode, error }) => {
    console.error(`${statusCode}: ${HttpStatus.getStatusText(statusCode)}`);

    const response: Error | undefined = error;
    if (response) {
      console.error(yaml.dump(response.error));
    }

    process.exit(1);
  });

export default withCatch;
