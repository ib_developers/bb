/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { BranchingModelSettingsAllOfLinksSelf } from './branchingModelSettingsAllOfLinksSelf';

export class RepositoryAllOfLinks {
    'forks'?: BranchingModelSettingsAllOfLinksSelf;
    'downloads'?: BranchingModelSettingsAllOfLinksSelf;
    'clone'?: Array<BranchingModelSettingsAllOfLinksSelf>;
    'watchers'?: BranchingModelSettingsAllOfLinksSelf;
    'commits'?: BranchingModelSettingsAllOfLinksSelf;
    'self'?: BranchingModelSettingsAllOfLinksSelf;
    'html'?: BranchingModelSettingsAllOfLinksSelf;
    'avatar'?: BranchingModelSettingsAllOfLinksSelf;
    'hooks'?: BranchingModelSettingsAllOfLinksSelf;
    'pullrequests'?: BranchingModelSettingsAllOfLinksSelf;

    static discriminator: string | undefined = undefined;

    static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            "name": "forks",
            "baseName": "forks",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "downloads",
            "baseName": "downloads",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "clone",
            "baseName": "clone",
            "type": "Array<BranchingModelSettingsAllOfLinksSelf>"
        },
        {
            "name": "watchers",
            "baseName": "watchers",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "commits",
            "baseName": "commits",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "self",
            "baseName": "self",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "html",
            "baseName": "html",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "avatar",
            "baseName": "avatar",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "hooks",
            "baseName": "hooks",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "pullrequests",
            "baseName": "pullrequests",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        }    ];

    static getAttributeTypeMap() {
        return RepositoryAllOfLinks.attributeTypeMap;
    }
}

