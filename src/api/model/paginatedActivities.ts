/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { Activity } from './activity';

/**
* A paged list of activities
*/
export class PaginatedActivities {
    'page'?: number;
    'previous'?: string;
    'pagelen'?: number;
    'size'?: number;
    'next'?: string;
    'values'?: Array<Activity>;

    static discriminator: string | undefined = undefined;

    static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            "name": "page",
            "baseName": "page",
            "type": "number"
        },
        {
            "name": "previous",
            "baseName": "previous",
            "type": "string"
        },
        {
            "name": "pagelen",
            "baseName": "pagelen",
            "type": "number"
        },
        {
            "name": "size",
            "baseName": "size",
            "type": "number"
        },
        {
            "name": "next",
            "baseName": "next",
            "type": "string"
        },
        {
            "name": "values",
            "baseName": "values",
            "type": "Array<Activity>"
        }    ];

    static getAttributeTypeMap() {
        return PaginatedActivities.attributeTypeMap;
    }
}

