/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { ProjectAllOfLinks } from './projectAllOfLinks';
import { Team } from './team';

/**
* A Bitbucket project.             Projects are used by teams to organize repositories.
*/
export class ProjectAllOf {
    'updated_on'?: Date;
    'created_on'?: Date;
    /**
    * The name of the project.
    */
    'name'?: string;
    /**
    * The project\'s key.
    */
    'key'?: string;
    'description'?: string;
    'owner'?: Team;
    /**
    *  Indicates whether the project is publicly accessible, or whether it is private to the team and consequently only visible to team members. Note that private projects cannot contain public repositories.
    */
    'is_private'?: boolean;
    'links'?: ProjectAllOfLinks;
    /**
    * The project\'s immutable id.
    */
    'uuid'?: string;

    static discriminator: string | undefined = undefined;

    static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            "name": "updated_on",
            "baseName": "updated_on",
            "type": "Date"
        },
        {
            "name": "created_on",
            "baseName": "created_on",
            "type": "Date"
        },
        {
            "name": "name",
            "baseName": "name",
            "type": "string"
        },
        {
            "name": "key",
            "baseName": "key",
            "type": "string"
        },
        {
            "name": "description",
            "baseName": "description",
            "type": "string"
        },
        {
            "name": "owner",
            "baseName": "owner",
            "type": "Team"
        },
        {
            "name": "is_private",
            "baseName": "is_private",
            "type": "boolean"
        },
        {
            "name": "links",
            "baseName": "links",
            "type": "ProjectAllOfLinks"
        },
        {
            "name": "uuid",
            "baseName": "uuid",
            "type": "string"
        }    ];

    static getAttributeTypeMap() {
        return ProjectAllOf.attributeTypeMap;
    }
}

