/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { ModelObject } from './modelObject';
import { PipelineKnownHostAllOf } from './pipelineKnownHostAllOf';
import { PipelineSshPublicKey } from './pipelineSshPublicKey';

export class PipelineKnownHost extends ModelObject {
    'public_key'?: PipelineSshPublicKey;
    /**
    * The UUID identifying the known host.
    */
    'uuid'?: string;
    /**
    * The hostname of the known host.
    */
    'hostname'?: string;

    static discriminator: string | undefined = undefined;

    static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            "name": "public_key",
            "baseName": "public_key",
            "type": "PipelineSshPublicKey"
        },
        {
            "name": "uuid",
            "baseName": "uuid",
            "type": "string"
        },
        {
            "name": "hostname",
            "baseName": "hostname",
            "type": "string"
        }    ];

    static getAttributeTypeMap() {
        return super.getAttributeTypeMap().concat(PipelineKnownHost.attributeTypeMap);
    }
}

