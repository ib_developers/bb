/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { Account } from './account';
import { AccountAllOfLinks } from './accountAllOfLinks';
import { UserAllOf } from './userAllOf';

export class User {
    'type': string;
    'display_name'?: string;
    'created_on'?: Date;
    'website'?: string;
    'has_2fa_enabled'?: boolean;
    'username'?: string;
    'uuid'?: string;
    /**
    * Account name defined by the owner. Should be used instead of the \"username\" field. Note that \"nickname\" cannot be used in place of \"username\" in URLs and queries, as \"nickname\" is not guaranteed to be unique.
    */
    'nickname'?: string;
    'links'?: AccountAllOfLinks;
    /**
    * The status of the account. Currently the only possible value is \"active\", but more values may be added in the future.
    */
    'account_status'?: string;
    /**
    * The user\'s Atlassian account ID.
    */
    'account_id'?: string;
    'is_staff'?: boolean;

    static discriminator: string | undefined = undefined;

    static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            "name": "type",
            "baseName": "type",
            "type": "string"
        },
        {
            "name": "display_name",
            "baseName": "display_name",
            "type": "string"
        },
        {
            "name": "created_on",
            "baseName": "created_on",
            "type": "Date"
        },
        {
            "name": "website",
            "baseName": "website",
            "type": "string"
        },
        {
            "name": "has_2fa_enabled",
            "baseName": "has_2fa_enabled",
            "type": "boolean"
        },
        {
            "name": "username",
            "baseName": "username",
            "type": "string"
        },
        {
            "name": "uuid",
            "baseName": "uuid",
            "type": "string"
        },
        {
            "name": "nickname",
            "baseName": "nickname",
            "type": "string"
        },
        {
            "name": "links",
            "baseName": "links",
            "type": "AccountAllOfLinks"
        },
        {
            "name": "account_status",
            "baseName": "account_status",
            "type": "string"
        },
        {
            "name": "account_id",
            "baseName": "account_id",
            "type": "string"
        },
        {
            "name": "is_staff",
            "baseName": "is_staff",
            "type": "boolean"
        }    ];

    static getAttributeTypeMap() {
        return User.attributeTypeMap;
    }
}

