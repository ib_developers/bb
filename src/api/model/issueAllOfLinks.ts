/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { BranchingModelSettingsAllOfLinksSelf } from './branchingModelSettingsAllOfLinksSelf';

export class IssueAllOfLinks {
    'attachments'?: BranchingModelSettingsAllOfLinksSelf;
    'comments'?: BranchingModelSettingsAllOfLinksSelf;
    'watch'?: BranchingModelSettingsAllOfLinksSelf;
    'self'?: BranchingModelSettingsAllOfLinksSelf;
    'html'?: BranchingModelSettingsAllOfLinksSelf;
    'vote'?: BranchingModelSettingsAllOfLinksSelf;

    static discriminator: string | undefined = undefined;

    static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            "name": "attachments",
            "baseName": "attachments",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "comments",
            "baseName": "comments",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "watch",
            "baseName": "watch",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "self",
            "baseName": "self",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "html",
            "baseName": "html",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        },
        {
            "name": "vote",
            "baseName": "vote",
            "type": "BranchingModelSettingsAllOfLinksSelf"
        }    ];

    static getAttributeTypeMap() {
        return IssueAllOfLinks.attributeTypeMap;
    }
}

