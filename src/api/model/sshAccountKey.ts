/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { Account } from './account';
import { BranchingModelSettingsAllOfLinks } from './branchingModelSettingsAllOfLinks';
import { SshAccountKeyAllOf } from './sshAccountKeyAllOf';
import { SshKey } from './sshKey';

export class SshAccountKey {
    'type': string;
    'last_used'?: Date;
    /**
    * The SSH public key value in OpenSSH format.
    */
    'key'?: string;
    /**
    * The comment parsed from the SSH key (if present)
    */
    'comment'?: string;
    'created_on'?: Date;
    /**
    * The SSH key\'s immutable ID.
    */
    'uuid'?: string;
    'links'?: BranchingModelSettingsAllOfLinks;
    /**
    * The user-defined label for the SSH key
    */
    'label'?: string;
    'owner'?: Account;

    static discriminator: string | undefined = undefined;

    static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            "name": "type",
            "baseName": "type",
            "type": "string"
        },
        {
            "name": "last_used",
            "baseName": "last_used",
            "type": "Date"
        },
        {
            "name": "key",
            "baseName": "key",
            "type": "string"
        },
        {
            "name": "comment",
            "baseName": "comment",
            "type": "string"
        },
        {
            "name": "created_on",
            "baseName": "created_on",
            "type": "Date"
        },
        {
            "name": "uuid",
            "baseName": "uuid",
            "type": "string"
        },
        {
            "name": "links",
            "baseName": "links",
            "type": "BranchingModelSettingsAllOfLinks"
        },
        {
            "name": "label",
            "baseName": "label",
            "type": "string"
        },
        {
            "name": "owner",
            "baseName": "owner",
            "type": "Account"
        }    ];

    static getAttributeTypeMap() {
        return SshAccountKey.attributeTypeMap;
    }
}

