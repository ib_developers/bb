import { writeFileSync } from 'fs';
import { Writable } from 'stream';

const get = <T>(param: string | boolean, response: T, outStream: Writable) => {
  const json = JSON.stringify(response, null, 2);

  if (typeof param === 'string') {
    writeFileSync(param, json);
  } else {
    outStream.write(json);
  }
};

export default get;
