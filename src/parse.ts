import { Command } from 'commander';
import moment from 'moment';
import { isUndefined, omitBy, pick } from 'lodash';
import parsers from './parsers';

const parse = (data: any, type: string, member?: string[] | null): any => {
  if (data === undefined || data === null) {
    return undefined;
  }

  const parser = parsers[type];

  if (parser) {
    let object = omitBy(
      parser.map.reduce(
        (accumulator, { name, type: elementType }) => ({
          ...accumulator,
          [name]: parse(data[name], elementType)
        }),
        {}
      ),
      isUndefined
    );

    if (member !== null) {
      object = pick(object, member || parser.default);
    }

    return Object.keys(object).length !== 0 ? object : undefined;
  }

  if (type.startsWith('Array<')) {
    const elementType = type.slice('Array<'.length, -'>'.length);
    return data.map((element: any) => parse(element, elementType));
  }

  if (type === 'Date') {
    return moment(data).fromNow();
  }

  if (type === 'string' || type === 'number' || type === 'boolean' || type.endsWith('Enum')) {
    return data;
  }

  throw new Error(`Unsupported parse type: ${type}`);
};

const withMember = (command: Command) => {
  let count = 0;

  return command.option('-m, --member <member>', undefined, (value: string, previous: string[]) => {
    const next = (count ? previous : []).concat([value]);
    count += 1;

    return next;
  });
};

export { parse, withMember };
