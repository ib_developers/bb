import rc from 'rc';
import { AuthOptions } from 'request';

interface Conf {
  auth?: Pick<AuthOptions, 'username'>;
  test?: {
    auth?: AuthOptions;
    username?: string;
    repoSlug?: string;
  };
}

const conf: Conf = rc('bb');

export default conf;
