# 貢献

はじめに、このドキュメントを読んでくださることに感謝します。
貢献の方法について紹介します。

## 課題の報告

[Bitbucket](https://bitbucket.org/ib_developers/bb/issues) によって課題を管理しています。
バグや機能強化、提案などの課題をお寄せください。

## 開発環境

次のコマンドによってソースコードをコンパイルします。

```console
$ npm run build
```

次のコマンドによってソースコードの変更を監視し、自動的にコンパイルします。

```console
$ npm run watch
```

Bitbucket API の型定義は [openapi-generator](https://github.com/OpenAPITools/openapi-generator) によって自動生成しています。
次のコマンドによって型定義を更新できます。

```console
$ npm run generate
```

## テスト

実際に Bitbucket Cloud を使用してテストを実行します。
テストを実行する場合は、[レートの制限](https://ja.confluence.atlassian.com/bitbucket/rate-limits-668173227.html)
などに注意のうえ、**各自の責任**となります。

テストで使用する Bitbucket アカウントを .bbrc に設定する必要があります。
**[アプリパスワード](https://ja.confluence.atlassian.com/bitbucket/app-passwords-828781300.html)の使用を推奨します。**
**認証情報の管理に注意してください。**
**username/repoSlug リポジトリはテストによって作成、更新、削除されます。**
**存在しないリポジトリ名を設定してください。**

```json
"test": {
  "auth": {
    "username": "test-auth-user",
    "password": "password" // アプリパスワード推奨
  },
  "username": "test-user",
  "repoSlug": "not-exist-repository" // 存在しないリポジトリ名
}
```

次のコマンドによってテストを実行します。

```console
$ npm test
```

テストではコードスタイルチェックや lint も実行します。
これらによるエラーは、コード整形によって自動的に修正できる場合があります。
次のコマンドによってコード整形および lint を実行します。

```console
$ npm run fix
```

## 開発フロー

[GitHub Flow](https://guides.github.com/introduction/flow/) を元に開発しています。
標準的な開発の流れは以下のとおりです。

- master ブランチから開発ブランチを作成します。
- 開発ブランチの最初のコミットを push します。
- master ブランチへプルリクエストを作成します。
- プルリクエストのタイトルの先頭に [WIP] をつけます。
- rebase で master ブランチと同期します。
- 開発が終了したら [WIP] を解除します。
- 必要があれば再度 [WIP] をつけて開発します。

## プルリクエストチェック

円滑なプルリクエストをするために、以下の条件を満たすことを推奨します。

- テストに合格している。ただし、テスト環境を導入できない場合などはこの限りではありません。
- master ブランチへ fast-forward マージできる。
- コミットおよびコミットメッセージが整備されている。
