"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const get = (param, response, outStream) => {
    const json = JSON.stringify(response, null, 2);
    if (typeof param === 'string') {
        fs_1.writeFileSync(param, json);
    }
    else {
        outStream.write(json);
    }
};
exports.default = get;
