"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
* An issue file attachment\'s meta data. Note this does not contain the file\'s actual contents.
*/
class IssueAttachmentAllOf {
    static getAttributeTypeMap() {
        return IssueAttachmentAllOf.attributeTypeMap;
    }
}
exports.IssueAttachmentAllOf = IssueAttachmentAllOf;
IssueAttachmentAllOf.discriminator = undefined;
IssueAttachmentAllOf.attributeTypeMap = [
    {
        "name": "links",
        "baseName": "links",
        "type": "BranchingModelSettingsAllOfLinks"
    },
    {
        "name": "name",
        "baseName": "name",
        "type": "string"
    }
];
