"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
class DeploymentStateCompletedStatusStopped {
    static getAttributeTypeMap() {
        return DeploymentStateCompletedStatusStopped.attributeTypeMap;
    }
}
exports.DeploymentStateCompletedStatusStopped = DeploymentStateCompletedStatusStopped;
DeploymentStateCompletedStatusStopped.discriminator = undefined;
DeploymentStateCompletedStatusStopped.attributeTypeMap = [
    {
        "name": "type",
        "baseName": "type",
        "type": "string"
    },
    {
        "name": "name",
        "baseName": "name",
        "type": "DeploymentStateCompletedStatusStopped.NameEnum"
    }
];
(function (DeploymentStateCompletedStatusStopped) {
    let NameEnum;
    (function (NameEnum) {
        NameEnum[NameEnum["STOPPED"] = 'STOPPED'] = "STOPPED";
    })(NameEnum = DeploymentStateCompletedStatusStopped.NameEnum || (DeploymentStateCompletedStatusStopped.NameEnum = {}));
})(DeploymentStateCompletedStatusStopped = exports.DeploymentStateCompletedStatusStopped || (exports.DeploymentStateCompletedStatusStopped = {}));
