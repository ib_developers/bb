"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
class RepositoryAllOfLinks {
    static getAttributeTypeMap() {
        return RepositoryAllOfLinks.attributeTypeMap;
    }
}
exports.RepositoryAllOfLinks = RepositoryAllOfLinks;
RepositoryAllOfLinks.discriminator = undefined;
RepositoryAllOfLinks.attributeTypeMap = [
    {
        "name": "forks",
        "baseName": "forks",
        "type": "BranchingModelSettingsAllOfLinksSelf"
    },
    {
        "name": "downloads",
        "baseName": "downloads",
        "type": "BranchingModelSettingsAllOfLinksSelf"
    },
    {
        "name": "clone",
        "baseName": "clone",
        "type": "Array<BranchingModelSettingsAllOfLinksSelf>"
    },
    {
        "name": "watchers",
        "baseName": "watchers",
        "type": "BranchingModelSettingsAllOfLinksSelf"
    },
    {
        "name": "commits",
        "baseName": "commits",
        "type": "BranchingModelSettingsAllOfLinksSelf"
    },
    {
        "name": "self",
        "baseName": "self",
        "type": "BranchingModelSettingsAllOfLinksSelf"
    },
    {
        "name": "html",
        "baseName": "html",
        "type": "BranchingModelSettingsAllOfLinksSelf"
    },
    {
        "name": "avatar",
        "baseName": "avatar",
        "type": "BranchingModelSettingsAllOfLinksSelf"
    },
    {
        "name": "hooks",
        "baseName": "hooks",
        "type": "BranchingModelSettingsAllOfLinksSelf"
    },
    {
        "name": "pullrequests",
        "baseName": "pullrequests",
        "type": "BranchingModelSettingsAllOfLinksSelf"
    }
];
