"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
* A Bitbucket Pipelines NOT_RUN pipeline step result.
*/
class PipelineStepStateCompletedNotRunAllOf {
    static getAttributeTypeMap() {
        return PipelineStepStateCompletedNotRunAllOf.attributeTypeMap;
    }
}
exports.PipelineStepStateCompletedNotRunAllOf = PipelineStepStateCompletedNotRunAllOf;
PipelineStepStateCompletedNotRunAllOf.discriminator = undefined;
PipelineStepStateCompletedNotRunAllOf.attributeTypeMap = [
    {
        "name": "name",
        "baseName": "name",
        "type": "PipelineStepStateCompletedNotRunAllOf.NameEnum"
    }
];
(function (PipelineStepStateCompletedNotRunAllOf) {
    let NameEnum;
    (function (NameEnum) {
        NameEnum[NameEnum["NOTRUN"] = 'NOT_RUN'] = "NOTRUN";
    })(NameEnum = PipelineStepStateCompletedNotRunAllOf.NameEnum || (PipelineStepStateCompletedNotRunAllOf.NameEnum = {}));
})(PipelineStepStateCompletedNotRunAllOf = exports.PipelineStepStateCompletedNotRunAllOf || (exports.PipelineStepStateCompletedNotRunAllOf = {}));
