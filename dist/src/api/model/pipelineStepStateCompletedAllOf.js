"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
* A Bitbucket Pipelines COMPLETED pipeline step state.
*/
class PipelineStepStateCompletedAllOf {
    static getAttributeTypeMap() {
        return PipelineStepStateCompletedAllOf.attributeTypeMap;
    }
}
exports.PipelineStepStateCompletedAllOf = PipelineStepStateCompletedAllOf;
PipelineStepStateCompletedAllOf.discriminator = undefined;
PipelineStepStateCompletedAllOf.attributeTypeMap = [
    {
        "name": "result",
        "baseName": "result",
        "type": "PipelineStepStateCompletedResult"
    },
    {
        "name": "name",
        "baseName": "name",
        "type": "PipelineStepStateCompletedAllOf.NameEnum"
    }
];
(function (PipelineStepStateCompletedAllOf) {
    let NameEnum;
    (function (NameEnum) {
        NameEnum[NameEnum["COMPLETED"] = 'COMPLETED'] = "COMPLETED";
    })(NameEnum = PipelineStepStateCompletedAllOf.NameEnum || (PipelineStepStateCompletedAllOf.NameEnum = {}));
})(PipelineStepStateCompletedAllOf = exports.PipelineStepStateCompletedAllOf || (exports.PipelineStepStateCompletedAllOf = {}));
