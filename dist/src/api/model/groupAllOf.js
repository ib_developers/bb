"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
* A group object
*/
class GroupAllOf {
    static getAttributeTypeMap() {
        return GroupAllOf.attributeTypeMap;
    }
}
exports.GroupAllOf = GroupAllOf;
GroupAllOf.discriminator = undefined;
GroupAllOf.attributeTypeMap = [
    {
        "name": "members",
        "baseName": "members",
        "type": "number"
    },
    {
        "name": "links",
        "baseName": "links",
        "type": "GroupAllOfLinks"
    },
    {
        "name": "slug",
        "baseName": "slug",
        "type": "string"
    },
    {
        "name": "full_slug",
        "baseName": "full_slug",
        "type": "string"
    },
    {
        "name": "name",
        "baseName": "name",
        "type": "string"
    },
    {
        "name": "owner",
        "baseName": "owner",
        "type": "Account"
    }
];
