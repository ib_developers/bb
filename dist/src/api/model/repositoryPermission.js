"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
* A user\'s permission for a given repository.
*/
class RepositoryPermission {
    static getAttributeTypeMap() {
        return RepositoryPermission.attributeTypeMap;
    }
}
exports.RepositoryPermission = RepositoryPermission;
RepositoryPermission.discriminator = undefined;
RepositoryPermission.attributeTypeMap = [
    {
        "name": "type",
        "baseName": "type",
        "type": "string"
    },
    {
        "name": "permission",
        "baseName": "permission",
        "type": "RepositoryPermission.PermissionEnum"
    },
    {
        "name": "repository",
        "baseName": "repository",
        "type": "Repository"
    },
    {
        "name": "user",
        "baseName": "user",
        "type": "User"
    }
];
(function (RepositoryPermission) {
    let PermissionEnum;
    (function (PermissionEnum) {
        PermissionEnum[PermissionEnum["Admin"] = 'admin'] = "Admin";
        PermissionEnum[PermissionEnum["Write"] = 'write'] = "Write";
        PermissionEnum[PermissionEnum["Read"] = 'read'] = "Read";
    })(PermissionEnum = RepositoryPermission.PermissionEnum || (RepositoryPermission.PermissionEnum = {}));
})(RepositoryPermission = exports.RepositoryPermission || (exports.RepositoryPermission = {}));
