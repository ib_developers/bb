"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
class PipelineStatePending {
    static getAttributeTypeMap() {
        return PipelineStatePending.attributeTypeMap;
    }
}
exports.PipelineStatePending = PipelineStatePending;
PipelineStatePending.discriminator = undefined;
PipelineStatePending.attributeTypeMap = [
    {
        "name": "type",
        "baseName": "type",
        "type": "string"
    },
    {
        "name": "name",
        "baseName": "name",
        "type": "PipelineStatePending.NameEnum"
    }
];
(function (PipelineStatePending) {
    let NameEnum;
    (function (NameEnum) {
        NameEnum[NameEnum["PENDING"] = 'PENDING'] = "PENDING";
    })(NameEnum = PipelineStatePending.NameEnum || (PipelineStatePending.NameEnum = {}));
})(PipelineStatePending = exports.PipelineStatePending || (exports.PipelineStatePending = {}));
