"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
* A paginated list of branches.
*/
class PaginatedBranches {
    static getAttributeTypeMap() {
        return PaginatedBranches.attributeTypeMap;
    }
}
exports.PaginatedBranches = PaginatedBranches;
PaginatedBranches.discriminator = undefined;
PaginatedBranches.attributeTypeMap = [
    {
        "name": "values",
        "baseName": "values",
        "type": "Array<Branch>"
    },
    {
        "name": "size",
        "baseName": "size",
        "type": "number"
    },
    {
        "name": "next",
        "baseName": "next",
        "type": "string"
    },
    {
        "name": "pagelen",
        "baseName": "pagelen",
        "type": "number"
    },
    {
        "name": "previous",
        "baseName": "previous",
        "type": "string"
    },
    {
        "name": "page",
        "baseName": "page",
        "type": "number"
    }
];
