"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
* A Bitbucket Pipelines reference target.
*/
class PipelineRefTargetAllOf {
    static getAttributeTypeMap() {
        return PipelineRefTargetAllOf.attributeTypeMap;
    }
}
exports.PipelineRefTargetAllOf = PipelineRefTargetAllOf;
PipelineRefTargetAllOf.discriminator = undefined;
PipelineRefTargetAllOf.attributeTypeMap = [
    {
        "name": "ref_type",
        "baseName": "ref_type",
        "type": "PipelineRefTargetAllOf.RefTypeEnum"
    },
    {
        "name": "commit",
        "baseName": "commit",
        "type": "Commit"
    },
    {
        "name": "ref_name",
        "baseName": "ref_name",
        "type": "string"
    },
    {
        "name": "selector",
        "baseName": "selector",
        "type": "PipelineSelector"
    }
];
(function (PipelineRefTargetAllOf) {
    let RefTypeEnum;
    (function (RefTypeEnum) {
        RefTypeEnum[RefTypeEnum["Branch"] = 'branch'] = "Branch";
        RefTypeEnum[RefTypeEnum["Tag"] = 'tag'] = "Tag";
        RefTypeEnum[RefTypeEnum["NamedBranch"] = 'named_branch'] = "NamedBranch";
        RefTypeEnum[RefTypeEnum["Bookmark"] = 'bookmark'] = "Bookmark";
    })(RefTypeEnum = PipelineRefTargetAllOf.RefTypeEnum || (PipelineRefTargetAllOf.RefTypeEnum = {}));
})(PipelineRefTargetAllOf = exports.PipelineRefTargetAllOf || (exports.PipelineRefTargetAllOf = {}));
