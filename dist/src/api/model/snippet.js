"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const modelObject_1 = require("./modelObject");
class Snippet extends modelObject_1.ModelObject {
    static getAttributeTypeMap() {
        return super.getAttributeTypeMap().concat(Snippet.attributeTypeMap);
    }
}
exports.Snippet = Snippet;
Snippet.discriminator = undefined;
Snippet.attributeTypeMap = [
    {
        "name": "is_private",
        "baseName": "is_private",
        "type": "boolean"
    },
    {
        "name": "creator",
        "baseName": "creator",
        "type": "Account"
    },
    {
        "name": "scm",
        "baseName": "scm",
        "type": "Snippet.ScmEnum"
    },
    {
        "name": "updated_on",
        "baseName": "updated_on",
        "type": "Date"
    },
    {
        "name": "id",
        "baseName": "id",
        "type": "number"
    },
    {
        "name": "created_on",
        "baseName": "created_on",
        "type": "Date"
    },
    {
        "name": "title",
        "baseName": "title",
        "type": "string"
    },
    {
        "name": "owner",
        "baseName": "owner",
        "type": "Account"
    }
];
(function (Snippet) {
    let ScmEnum;
    (function (ScmEnum) {
        ScmEnum[ScmEnum["Hg"] = 'hg'] = "Hg";
        ScmEnum[ScmEnum["Git"] = 'git'] = "Git";
    })(ScmEnum = Snippet.ScmEnum || (Snippet.ScmEnum = {}));
})(Snippet = exports.Snippet || (exports.Snippet = {}));
