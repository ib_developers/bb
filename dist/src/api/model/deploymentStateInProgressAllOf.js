"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
* A Bitbucket Deployment IN_PROGRESS deployment state.
*/
class DeploymentStateInProgressAllOf {
    static getAttributeTypeMap() {
        return DeploymentStateInProgressAllOf.attributeTypeMap;
    }
}
exports.DeploymentStateInProgressAllOf = DeploymentStateInProgressAllOf;
DeploymentStateInProgressAllOf.discriminator = undefined;
DeploymentStateInProgressAllOf.attributeTypeMap = [
    {
        "name": "name",
        "baseName": "name",
        "type": "DeploymentStateInProgressAllOf.NameEnum"
    },
    {
        "name": "url",
        "baseName": "url",
        "type": "string"
    },
    {
        "name": "start_date",
        "baseName": "start_date",
        "type": "Date"
    },
    {
        "name": "deployer",
        "baseName": "deployer",
        "type": "Account"
    }
];
(function (DeploymentStateInProgressAllOf) {
    let NameEnum;
    (function (NameEnum) {
        NameEnum[NameEnum["INPROGRESS"] = 'IN_PROGRESS'] = "INPROGRESS";
    })(NameEnum = DeploymentStateInProgressAllOf.NameEnum || (DeploymentStateInProgressAllOf.NameEnum = {}));
})(DeploymentStateInProgressAllOf = exports.DeploymentStateInProgressAllOf || (exports.DeploymentStateInProgressAllOf = {}));
