"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
* A Bitbucket Pipelines ERROR pipeline result.
*/
class PipelineStateCompletedErrorAllOf {
    static getAttributeTypeMap() {
        return PipelineStateCompletedErrorAllOf.attributeTypeMap;
    }
}
exports.PipelineStateCompletedErrorAllOf = PipelineStateCompletedErrorAllOf;
PipelineStateCompletedErrorAllOf.discriminator = undefined;
PipelineStateCompletedErrorAllOf.attributeTypeMap = [
    {
        "name": "name",
        "baseName": "name",
        "type": "PipelineStateCompletedErrorAllOf.NameEnum"
    },
    {
        "name": "error",
        "baseName": "error",
        "type": "PipelineError"
    }
];
(function (PipelineStateCompletedErrorAllOf) {
    let NameEnum;
    (function (NameEnum) {
        NameEnum[NameEnum["ERROR"] = 'ERROR'] = "ERROR";
    })(NameEnum = PipelineStateCompletedErrorAllOf.NameEnum || (PipelineStateCompletedErrorAllOf.NameEnum = {}));
})(PipelineStateCompletedErrorAllOf = exports.PipelineStateCompletedErrorAllOf || (exports.PipelineStateCompletedErrorAllOf = {}));
