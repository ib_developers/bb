"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
* A Bitbucket project.             Projects are used by teams to organize repositories.
*/
class ProjectAllOf {
    static getAttributeTypeMap() {
        return ProjectAllOf.attributeTypeMap;
    }
}
exports.ProjectAllOf = ProjectAllOf;
ProjectAllOf.discriminator = undefined;
ProjectAllOf.attributeTypeMap = [
    {
        "name": "updated_on",
        "baseName": "updated_on",
        "type": "Date"
    },
    {
        "name": "created_on",
        "baseName": "created_on",
        "type": "Date"
    },
    {
        "name": "name",
        "baseName": "name",
        "type": "string"
    },
    {
        "name": "key",
        "baseName": "key",
        "type": "string"
    },
    {
        "name": "description",
        "baseName": "description",
        "type": "string"
    },
    {
        "name": "owner",
        "baseName": "owner",
        "type": "Team"
    },
    {
        "name": "is_private",
        "baseName": "is_private",
        "type": "boolean"
    },
    {
        "name": "links",
        "baseName": "links",
        "type": "ProjectAllOfLinks"
    },
    {
        "name": "uuid",
        "baseName": "uuid",
        "type": "string"
    }
];
