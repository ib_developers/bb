"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./account"));
__export(require("./accountAllOf"));
__export(require("./accountAllOfLinks"));
__export(require("./activity"));
__export(require("./author"));
__export(require("./authorAllOf"));
__export(require("./baseCommit"));
__export(require("./baseCommitAllOf"));
__export(require("./branch"));
__export(require("./branchingModel"));
__export(require("./branchingModelAllOf"));
__export(require("./branchingModelAllOfBranchTypes"));
__export(require("./branchingModelAllOfProduction"));
__export(require("./branchingModelSettings"));
__export(require("./branchingModelSettingsAllOf"));
__export(require("./branchingModelSettingsAllOfBranchTypes"));
__export(require("./branchingModelSettingsAllOfDevelopment"));
__export(require("./branchingModelSettingsAllOfLinks"));
__export(require("./branchingModelSettingsAllOfLinksSelf"));
__export(require("./branchingModelSettingsAllOfProduction"));
__export(require("./branchrestriction"));
__export(require("./branchrestrictionAllOf"));
__export(require("./comment"));
__export(require("./commentAllOf"));
__export(require("./commentAllOfLinks"));
__export(require("./commentContent"));
__export(require("./commentInline"));
__export(require("./commit"));
__export(require("./commitAllOf"));
__export(require("./commitComment"));
__export(require("./commitCommentAllOf"));
__export(require("./commitFile"));
__export(require("./commitstatus"));
__export(require("./commitstatusAllOf"));
__export(require("./commitstatusAllOfLinks"));
__export(require("./component"));
__export(require("./componentAllOf"));
__export(require("./deployKey"));
__export(require("./deployKeyAllOf"));
__export(require("./deployment"));
__export(require("./deploymentAllOf"));
__export(require("./deploymentEnvironment"));
__export(require("./deploymentEnvironmentAllOf"));
__export(require("./deploymentRelease"));
__export(require("./deploymentReleaseAllOf"));
__export(require("./deploymentState"));
__export(require("./deploymentStateCompleted"));
__export(require("./deploymentStateCompletedAllOf"));
__export(require("./deploymentStateCompletedStatus"));
__export(require("./deploymentStateCompletedStatusFailed"));
__export(require("./deploymentStateCompletedStatusFailedAllOf"));
__export(require("./deploymentStateCompletedStatusStopped"));
__export(require("./deploymentStateCompletedStatusStoppedAllOf"));
__export(require("./deploymentStateCompletedStatusSuccessful"));
__export(require("./deploymentStateCompletedStatusSuccessfulAllOf"));
__export(require("./deploymentStateInProgress"));
__export(require("./deploymentStateInProgressAllOf"));
__export(require("./deploymentStateUndeployed"));
__export(require("./deploymentStateUndeployedAllOf"));
__export(require("./diffstat"));
__export(require("./error"));
__export(require("./errorError"));
__export(require("./group"));
__export(require("./groupAllOf"));
__export(require("./groupAllOfLinks"));
__export(require("./hookEvent"));
__export(require("./issue"));
__export(require("./issueAllOf"));
__export(require("./issueAllOfContent"));
__export(require("./issueAllOfLinks"));
__export(require("./issueAttachment"));
__export(require("./issueAttachmentAllOf"));
__export(require("./issueChange"));
__export(require("./issueChangeChanges"));
__export(require("./issueChangeChangesComponent"));
__export(require("./issueChangeLinks"));
__export(require("./issueChangeLinksIssue"));
__export(require("./issueChangeMessage"));
__export(require("./issueComment"));
__export(require("./issueCommentAllOf"));
__export(require("./issueJobStatus"));
__export(require("./milestone"));
__export(require("./milestoneAllOf"));
__export(require("./modelObject"));
__export(require("./page"));
__export(require("./paginatedActivities"));
__export(require("./paginatedBranches"));
__export(require("./paginatedBranchrestrictions"));
__export(require("./paginatedCommitComments"));
__export(require("./paginatedCommitstatuses"));
__export(require("./paginatedComponents"));
__export(require("./paginatedDeployKeys"));
__export(require("./paginatedDeployments"));
__export(require("./paginatedDiffstats"));
__export(require("./paginatedEnvironments"));
__export(require("./paginatedFiles"));
__export(require("./paginatedHookEvents"));
__export(require("./paginatedIssueAttachments"));
__export(require("./paginatedIssueComments"));
__export(require("./paginatedIssues"));
__export(require("./paginatedLogEntries"));
__export(require("./paginatedMilestones"));
__export(require("./paginatedPipelineKnownHosts"));
__export(require("./paginatedPipelineScheduleExecutions"));
__export(require("./paginatedPipelineSchedules"));
__export(require("./paginatedPipelineSteps"));
__export(require("./paginatedPipelineVariables"));
__export(require("./paginatedPipelines"));
__export(require("./paginatedProjects"));
__export(require("./paginatedPullrequestComments"));
__export(require("./paginatedPullrequests"));
__export(require("./paginatedPullrequestsCommits"));
__export(require("./paginatedRefs"));
__export(require("./paginatedRepositories"));
__export(require("./paginatedRepositoryPermissions"));
__export(require("./paginatedSnippetComments"));
__export(require("./paginatedSnippetCommit"));
__export(require("./paginatedSnippets"));
__export(require("./paginatedSshUserKeys"));
__export(require("./paginatedTags"));
__export(require("./paginatedTeamPermissions"));
__export(require("./paginatedTeams"));
__export(require("./paginatedTreeentries"));
__export(require("./paginatedUsers"));
__export(require("./paginatedVersions"));
__export(require("./paginatedWebhookSubscriptions"));
__export(require("./participant"));
__export(require("./participantAllOf"));
__export(require("./pipeline"));
__export(require("./pipelineAllOf"));
__export(require("./pipelineBuildNumber"));
__export(require("./pipelineBuildNumberAllOf"));
__export(require("./pipelineCommand"));
__export(require("./pipelineCommitTarget"));
__export(require("./pipelineCommitTargetAllOf"));
__export(require("./pipelineError"));
__export(require("./pipelineErrorAllOf"));
__export(require("./pipelineImage"));
__export(require("./pipelineKnownHost"));
__export(require("./pipelineKnownHostAllOf"));
__export(require("./pipelineRefTarget"));
__export(require("./pipelineRefTargetAllOf"));
__export(require("./pipelineSchedule"));
__export(require("./pipelineScheduleAllOf"));
__export(require("./pipelineScheduleExecution"));
__export(require("./pipelineScheduleExecutionErrored"));
__export(require("./pipelineScheduleExecutionErroredAllOf"));
__export(require("./pipelineScheduleExecutionExecuted"));
__export(require("./pipelineScheduleExecutionExecutedAllOf"));
__export(require("./pipelineSelector"));
__export(require("./pipelineSshKeyPair"));
__export(require("./pipelineSshKeyPairAllOf"));
__export(require("./pipelineSshPublicKey"));
__export(require("./pipelineSshPublicKeyAllOf"));
__export(require("./pipelineState"));
__export(require("./pipelineStateCompleted"));
__export(require("./pipelineStateCompletedAllOf"));
__export(require("./pipelineStateCompletedError"));
__export(require("./pipelineStateCompletedErrorAllOf"));
__export(require("./pipelineStateCompletedExpired"));
__export(require("./pipelineStateCompletedExpiredAllOf"));
__export(require("./pipelineStateCompletedFailed"));
__export(require("./pipelineStateCompletedFailedAllOf"));
__export(require("./pipelineStateCompletedResult"));
__export(require("./pipelineStateCompletedStopped"));
__export(require("./pipelineStateCompletedStoppedAllOf"));
__export(require("./pipelineStateCompletedSuccessful"));
__export(require("./pipelineStateCompletedSuccessfulAllOf"));
__export(require("./pipelineStateInProgress"));
__export(require("./pipelineStateInProgressAllOf"));
__export(require("./pipelineStateInProgressPaused"));
__export(require("./pipelineStateInProgressPausedAllOf"));
__export(require("./pipelineStateInProgressRunning"));
__export(require("./pipelineStateInProgressRunningAllOf"));
__export(require("./pipelineStateInProgressStage"));
__export(require("./pipelineStatePending"));
__export(require("./pipelineStatePendingAllOf"));
__export(require("./pipelineStep"));
__export(require("./pipelineStepAllOf"));
__export(require("./pipelineStepError"));
__export(require("./pipelineStepErrorAllOf"));
__export(require("./pipelineStepState"));
__export(require("./pipelineStepStateCompleted"));
__export(require("./pipelineStepStateCompletedAllOf"));
__export(require("./pipelineStepStateCompletedError"));
__export(require("./pipelineStepStateCompletedErrorAllOf"));
__export(require("./pipelineStepStateCompletedExpired"));
__export(require("./pipelineStepStateCompletedExpiredAllOf"));
__export(require("./pipelineStepStateCompletedFailed"));
__export(require("./pipelineStepStateCompletedFailedAllOf"));
__export(require("./pipelineStepStateCompletedNotRun"));
__export(require("./pipelineStepStateCompletedNotRunAllOf"));
__export(require("./pipelineStepStateCompletedResult"));
__export(require("./pipelineStepStateCompletedStopped"));
__export(require("./pipelineStepStateCompletedStoppedAllOf"));
__export(require("./pipelineStepStateCompletedSuccessful"));
__export(require("./pipelineStepStateCompletedSuccessfulAllOf"));
__export(require("./pipelineStepStateInProgress"));
__export(require("./pipelineStepStateInProgressAllOf"));
__export(require("./pipelineStepStatePending"));
__export(require("./pipelineStepStatePendingAllOf"));
__export(require("./pipelineStepStateReady"));
__export(require("./pipelineStepStateReadyAllOf"));
__export(require("./pipelineTarget"));
__export(require("./pipelineTrigger"));
__export(require("./pipelineTriggerManual"));
__export(require("./pipelineTriggerPush"));
__export(require("./pipelineVariable"));
__export(require("./pipelineVariableAllOf"));
__export(require("./pipelinesConfig"));
__export(require("./pipelinesConfigAllOf"));
__export(require("./pipelinesDdevPipelineStep"));
__export(require("./pipelinesStgWestPipelineStep"));
__export(require("./project"));
__export(require("./projectAllOf"));
__export(require("./projectAllOfLinks"));
__export(require("./pullrequest"));
__export(require("./pullrequestAllOf"));
__export(require("./pullrequestAllOfLinks"));
__export(require("./pullrequestAllOfMergeCommit"));
__export(require("./pullrequestAllOfRendered"));
__export(require("./pullrequestEndpoint"));
__export(require("./pullrequestEndpointBranch"));
__export(require("./pullrequestEndpointCommit"));
__export(require("./pullrequestMergeParameters"));
__export(require("./ref"));
__export(require("./refLinks"));
__export(require("./repository"));
__export(require("./repositoryAllOf"));
__export(require("./repositoryAllOfLinks"));
__export(require("./repositoryPermission"));
__export(require("./searchCodeSearchResult"));
__export(require("./searchContentMatch"));
__export(require("./searchLine"));
__export(require("./searchResultPage"));
__export(require("./searchSegment"));
__export(require("./snippet"));
__export(require("./snippetAllOf"));
__export(require("./snippetComment"));
__export(require("./snippetCommentAllOf"));
__export(require("./snippetCommit"));
__export(require("./snippetCommitAllOf"));
__export(require("./snippetCommitAllOfLinks"));
__export(require("./sshAccountKey"));
__export(require("./sshAccountKeyAllOf"));
__export(require("./sshKey"));
__export(require("./sshKeyAllOf"));
__export(require("./subjectTypes"));
__export(require("./subjectTypesRepository"));
__export(require("./tag"));
__export(require("./tagAllOf"));
__export(require("./team"));
__export(require("./teamPermission"));
__export(require("./treeentry"));
__export(require("./user"));
__export(require("./userAllOf"));
__export(require("./version"));
__export(require("./versionAllOf"));
__export(require("./webhookSubscription"));
__export(require("./webhookSubscriptionAllOf"));
const account_1 = require("./account");
const accountAllOf_1 = require("./accountAllOf");
const accountAllOfLinks_1 = require("./accountAllOfLinks");
const activity_1 = require("./activity");
const author_1 = require("./author");
const authorAllOf_1 = require("./authorAllOf");
const baseCommit_1 = require("./baseCommit");
const baseCommitAllOf_1 = require("./baseCommitAllOf");
const branch_1 = require("./branch");
const branchingModel_1 = require("./branchingModel");
const branchingModelAllOf_1 = require("./branchingModelAllOf");
const branchingModelAllOfBranchTypes_1 = require("./branchingModelAllOfBranchTypes");
const branchingModelAllOfProduction_1 = require("./branchingModelAllOfProduction");
const branchingModelSettings_1 = require("./branchingModelSettings");
const branchingModelSettingsAllOf_1 = require("./branchingModelSettingsAllOf");
const branchingModelSettingsAllOfBranchTypes_1 = require("./branchingModelSettingsAllOfBranchTypes");
const branchingModelSettingsAllOfDevelopment_1 = require("./branchingModelSettingsAllOfDevelopment");
const branchingModelSettingsAllOfLinks_1 = require("./branchingModelSettingsAllOfLinks");
const branchingModelSettingsAllOfLinksSelf_1 = require("./branchingModelSettingsAllOfLinksSelf");
const branchingModelSettingsAllOfProduction_1 = require("./branchingModelSettingsAllOfProduction");
const branchrestriction_1 = require("./branchrestriction");
const branchrestrictionAllOf_1 = require("./branchrestrictionAllOf");
const comment_1 = require("./comment");
const commentAllOf_1 = require("./commentAllOf");
const commentAllOfLinks_1 = require("./commentAllOfLinks");
const commentContent_1 = require("./commentContent");
const commentInline_1 = require("./commentInline");
const commit_1 = require("./commit");
const commitAllOf_1 = require("./commitAllOf");
const commitComment_1 = require("./commitComment");
const commitCommentAllOf_1 = require("./commitCommentAllOf");
const commitFile_1 = require("./commitFile");
const commitstatus_1 = require("./commitstatus");
const commitstatusAllOf_1 = require("./commitstatusAllOf");
const commitstatusAllOfLinks_1 = require("./commitstatusAllOfLinks");
const component_1 = require("./component");
const componentAllOf_1 = require("./componentAllOf");
const deployKey_1 = require("./deployKey");
const deployKeyAllOf_1 = require("./deployKeyAllOf");
const deployment_1 = require("./deployment");
const deploymentAllOf_1 = require("./deploymentAllOf");
const deploymentEnvironment_1 = require("./deploymentEnvironment");
const deploymentEnvironmentAllOf_1 = require("./deploymentEnvironmentAllOf");
const deploymentRelease_1 = require("./deploymentRelease");
const deploymentReleaseAllOf_1 = require("./deploymentReleaseAllOf");
const deploymentState_1 = require("./deploymentState");
const deploymentStateCompleted_1 = require("./deploymentStateCompleted");
const deploymentStateCompletedAllOf_1 = require("./deploymentStateCompletedAllOf");
const deploymentStateCompletedStatus_1 = require("./deploymentStateCompletedStatus");
const deploymentStateCompletedStatusFailed_1 = require("./deploymentStateCompletedStatusFailed");
const deploymentStateCompletedStatusFailedAllOf_1 = require("./deploymentStateCompletedStatusFailedAllOf");
const deploymentStateCompletedStatusStopped_1 = require("./deploymentStateCompletedStatusStopped");
const deploymentStateCompletedStatusStoppedAllOf_1 = require("./deploymentStateCompletedStatusStoppedAllOf");
const deploymentStateCompletedStatusSuccessful_1 = require("./deploymentStateCompletedStatusSuccessful");
const deploymentStateCompletedStatusSuccessfulAllOf_1 = require("./deploymentStateCompletedStatusSuccessfulAllOf");
const deploymentStateInProgress_1 = require("./deploymentStateInProgress");
const deploymentStateInProgressAllOf_1 = require("./deploymentStateInProgressAllOf");
const deploymentStateUndeployed_1 = require("./deploymentStateUndeployed");
const deploymentStateUndeployedAllOf_1 = require("./deploymentStateUndeployedAllOf");
const diffstat_1 = require("./diffstat");
const error_1 = require("./error");
const errorError_1 = require("./errorError");
const group_1 = require("./group");
const groupAllOf_1 = require("./groupAllOf");
const groupAllOfLinks_1 = require("./groupAllOfLinks");
const hookEvent_1 = require("./hookEvent");
const issue_1 = require("./issue");
const issueAllOf_1 = require("./issueAllOf");
const issueAllOfContent_1 = require("./issueAllOfContent");
const issueAllOfLinks_1 = require("./issueAllOfLinks");
const issueAttachment_1 = require("./issueAttachment");
const issueAttachmentAllOf_1 = require("./issueAttachmentAllOf");
const issueChange_1 = require("./issueChange");
const issueChangeChanges_1 = require("./issueChangeChanges");
const issueChangeChangesComponent_1 = require("./issueChangeChangesComponent");
const issueChangeLinks_1 = require("./issueChangeLinks");
const issueChangeLinksIssue_1 = require("./issueChangeLinksIssue");
const issueChangeMessage_1 = require("./issueChangeMessage");
const issueComment_1 = require("./issueComment");
const issueCommentAllOf_1 = require("./issueCommentAllOf");
const issueJobStatus_1 = require("./issueJobStatus");
const milestone_1 = require("./milestone");
const milestoneAllOf_1 = require("./milestoneAllOf");
const modelObject_1 = require("./modelObject");
const page_1 = require("./page");
const paginatedActivities_1 = require("./paginatedActivities");
const paginatedBranches_1 = require("./paginatedBranches");
const paginatedBranchrestrictions_1 = require("./paginatedBranchrestrictions");
const paginatedCommitComments_1 = require("./paginatedCommitComments");
const paginatedCommitstatuses_1 = require("./paginatedCommitstatuses");
const paginatedComponents_1 = require("./paginatedComponents");
const paginatedDeployKeys_1 = require("./paginatedDeployKeys");
const paginatedDeployments_1 = require("./paginatedDeployments");
const paginatedDiffstats_1 = require("./paginatedDiffstats");
const paginatedEnvironments_1 = require("./paginatedEnvironments");
const paginatedFiles_1 = require("./paginatedFiles");
const paginatedHookEvents_1 = require("./paginatedHookEvents");
const paginatedIssueAttachments_1 = require("./paginatedIssueAttachments");
const paginatedIssueComments_1 = require("./paginatedIssueComments");
const paginatedIssues_1 = require("./paginatedIssues");
const paginatedLogEntries_1 = require("./paginatedLogEntries");
const paginatedMilestones_1 = require("./paginatedMilestones");
const paginatedPipelineKnownHosts_1 = require("./paginatedPipelineKnownHosts");
const paginatedPipelineScheduleExecutions_1 = require("./paginatedPipelineScheduleExecutions");
const paginatedPipelineSchedules_1 = require("./paginatedPipelineSchedules");
const paginatedPipelineSteps_1 = require("./paginatedPipelineSteps");
const paginatedPipelineVariables_1 = require("./paginatedPipelineVariables");
const paginatedPipelines_1 = require("./paginatedPipelines");
const paginatedProjects_1 = require("./paginatedProjects");
const paginatedPullrequestComments_1 = require("./paginatedPullrequestComments");
const paginatedPullrequests_1 = require("./paginatedPullrequests");
const paginatedPullrequestsCommits_1 = require("./paginatedPullrequestsCommits");
const paginatedRefs_1 = require("./paginatedRefs");
const paginatedRepositories_1 = require("./paginatedRepositories");
const paginatedRepositoryPermissions_1 = require("./paginatedRepositoryPermissions");
const paginatedSnippetComments_1 = require("./paginatedSnippetComments");
const paginatedSnippetCommit_1 = require("./paginatedSnippetCommit");
const paginatedSnippets_1 = require("./paginatedSnippets");
const paginatedSshUserKeys_1 = require("./paginatedSshUserKeys");
const paginatedTags_1 = require("./paginatedTags");
const paginatedTeamPermissions_1 = require("./paginatedTeamPermissions");
const paginatedTeams_1 = require("./paginatedTeams");
const paginatedTreeentries_1 = require("./paginatedTreeentries");
const paginatedUsers_1 = require("./paginatedUsers");
const paginatedVersions_1 = require("./paginatedVersions");
const paginatedWebhookSubscriptions_1 = require("./paginatedWebhookSubscriptions");
const participant_1 = require("./participant");
const participantAllOf_1 = require("./participantAllOf");
const pipeline_1 = require("./pipeline");
const pipelineAllOf_1 = require("./pipelineAllOf");
const pipelineBuildNumber_1 = require("./pipelineBuildNumber");
const pipelineBuildNumberAllOf_1 = require("./pipelineBuildNumberAllOf");
const pipelineCommand_1 = require("./pipelineCommand");
const pipelineCommitTarget_1 = require("./pipelineCommitTarget");
const pipelineCommitTargetAllOf_1 = require("./pipelineCommitTargetAllOf");
const pipelineError_1 = require("./pipelineError");
const pipelineErrorAllOf_1 = require("./pipelineErrorAllOf");
const pipelineImage_1 = require("./pipelineImage");
const pipelineKnownHost_1 = require("./pipelineKnownHost");
const pipelineKnownHostAllOf_1 = require("./pipelineKnownHostAllOf");
const pipelineRefTarget_1 = require("./pipelineRefTarget");
const pipelineRefTargetAllOf_1 = require("./pipelineRefTargetAllOf");
const pipelineSchedule_1 = require("./pipelineSchedule");
const pipelineScheduleAllOf_1 = require("./pipelineScheduleAllOf");
const pipelineScheduleExecution_1 = require("./pipelineScheduleExecution");
const pipelineScheduleExecutionErrored_1 = require("./pipelineScheduleExecutionErrored");
const pipelineScheduleExecutionErroredAllOf_1 = require("./pipelineScheduleExecutionErroredAllOf");
const pipelineScheduleExecutionExecuted_1 = require("./pipelineScheduleExecutionExecuted");
const pipelineScheduleExecutionExecutedAllOf_1 = require("./pipelineScheduleExecutionExecutedAllOf");
const pipelineSelector_1 = require("./pipelineSelector");
const pipelineSshKeyPair_1 = require("./pipelineSshKeyPair");
const pipelineSshKeyPairAllOf_1 = require("./pipelineSshKeyPairAllOf");
const pipelineSshPublicKey_1 = require("./pipelineSshPublicKey");
const pipelineSshPublicKeyAllOf_1 = require("./pipelineSshPublicKeyAllOf");
const pipelineState_1 = require("./pipelineState");
const pipelineStateCompleted_1 = require("./pipelineStateCompleted");
const pipelineStateCompletedAllOf_1 = require("./pipelineStateCompletedAllOf");
const pipelineStateCompletedError_1 = require("./pipelineStateCompletedError");
const pipelineStateCompletedErrorAllOf_1 = require("./pipelineStateCompletedErrorAllOf");
const pipelineStateCompletedExpired_1 = require("./pipelineStateCompletedExpired");
const pipelineStateCompletedExpiredAllOf_1 = require("./pipelineStateCompletedExpiredAllOf");
const pipelineStateCompletedFailed_1 = require("./pipelineStateCompletedFailed");
const pipelineStateCompletedFailedAllOf_1 = require("./pipelineStateCompletedFailedAllOf");
const pipelineStateCompletedResult_1 = require("./pipelineStateCompletedResult");
const pipelineStateCompletedStopped_1 = require("./pipelineStateCompletedStopped");
const pipelineStateCompletedStoppedAllOf_1 = require("./pipelineStateCompletedStoppedAllOf");
const pipelineStateCompletedSuccessful_1 = require("./pipelineStateCompletedSuccessful");
const pipelineStateCompletedSuccessfulAllOf_1 = require("./pipelineStateCompletedSuccessfulAllOf");
const pipelineStateInProgress_1 = require("./pipelineStateInProgress");
const pipelineStateInProgressAllOf_1 = require("./pipelineStateInProgressAllOf");
const pipelineStateInProgressPaused_1 = require("./pipelineStateInProgressPaused");
const pipelineStateInProgressPausedAllOf_1 = require("./pipelineStateInProgressPausedAllOf");
const pipelineStateInProgressRunning_1 = require("./pipelineStateInProgressRunning");
const pipelineStateInProgressRunningAllOf_1 = require("./pipelineStateInProgressRunningAllOf");
const pipelineStateInProgressStage_1 = require("./pipelineStateInProgressStage");
const pipelineStatePending_1 = require("./pipelineStatePending");
const pipelineStatePendingAllOf_1 = require("./pipelineStatePendingAllOf");
const pipelineStep_1 = require("./pipelineStep");
const pipelineStepAllOf_1 = require("./pipelineStepAllOf");
const pipelineStepError_1 = require("./pipelineStepError");
const pipelineStepErrorAllOf_1 = require("./pipelineStepErrorAllOf");
const pipelineStepState_1 = require("./pipelineStepState");
const pipelineStepStateCompleted_1 = require("./pipelineStepStateCompleted");
const pipelineStepStateCompletedAllOf_1 = require("./pipelineStepStateCompletedAllOf");
const pipelineStepStateCompletedError_1 = require("./pipelineStepStateCompletedError");
const pipelineStepStateCompletedErrorAllOf_1 = require("./pipelineStepStateCompletedErrorAllOf");
const pipelineStepStateCompletedExpired_1 = require("./pipelineStepStateCompletedExpired");
const pipelineStepStateCompletedExpiredAllOf_1 = require("./pipelineStepStateCompletedExpiredAllOf");
const pipelineStepStateCompletedFailed_1 = require("./pipelineStepStateCompletedFailed");
const pipelineStepStateCompletedFailedAllOf_1 = require("./pipelineStepStateCompletedFailedAllOf");
const pipelineStepStateCompletedNotRun_1 = require("./pipelineStepStateCompletedNotRun");
const pipelineStepStateCompletedNotRunAllOf_1 = require("./pipelineStepStateCompletedNotRunAllOf");
const pipelineStepStateCompletedResult_1 = require("./pipelineStepStateCompletedResult");
const pipelineStepStateCompletedStopped_1 = require("./pipelineStepStateCompletedStopped");
const pipelineStepStateCompletedStoppedAllOf_1 = require("./pipelineStepStateCompletedStoppedAllOf");
const pipelineStepStateCompletedSuccessful_1 = require("./pipelineStepStateCompletedSuccessful");
const pipelineStepStateCompletedSuccessfulAllOf_1 = require("./pipelineStepStateCompletedSuccessfulAllOf");
const pipelineStepStateInProgress_1 = require("./pipelineStepStateInProgress");
const pipelineStepStateInProgressAllOf_1 = require("./pipelineStepStateInProgressAllOf");
const pipelineStepStatePending_1 = require("./pipelineStepStatePending");
const pipelineStepStatePendingAllOf_1 = require("./pipelineStepStatePendingAllOf");
const pipelineStepStateReady_1 = require("./pipelineStepStateReady");
const pipelineStepStateReadyAllOf_1 = require("./pipelineStepStateReadyAllOf");
const pipelineTarget_1 = require("./pipelineTarget");
const pipelineTrigger_1 = require("./pipelineTrigger");
const pipelineTriggerManual_1 = require("./pipelineTriggerManual");
const pipelineTriggerPush_1 = require("./pipelineTriggerPush");
const pipelineVariable_1 = require("./pipelineVariable");
const pipelineVariableAllOf_1 = require("./pipelineVariableAllOf");
const pipelinesConfig_1 = require("./pipelinesConfig");
const pipelinesConfigAllOf_1 = require("./pipelinesConfigAllOf");
const pipelinesDdevPipelineStep_1 = require("./pipelinesDdevPipelineStep");
const pipelinesStgWestPipelineStep_1 = require("./pipelinesStgWestPipelineStep");
const project_1 = require("./project");
const projectAllOf_1 = require("./projectAllOf");
const projectAllOfLinks_1 = require("./projectAllOfLinks");
const pullrequest_1 = require("./pullrequest");
const pullrequestAllOf_1 = require("./pullrequestAllOf");
const pullrequestAllOfLinks_1 = require("./pullrequestAllOfLinks");
const pullrequestAllOfMergeCommit_1 = require("./pullrequestAllOfMergeCommit");
const pullrequestAllOfRendered_1 = require("./pullrequestAllOfRendered");
const pullrequestEndpoint_1 = require("./pullrequestEndpoint");
const pullrequestEndpointBranch_1 = require("./pullrequestEndpointBranch");
const pullrequestEndpointCommit_1 = require("./pullrequestEndpointCommit");
const pullrequestMergeParameters_1 = require("./pullrequestMergeParameters");
const ref_1 = require("./ref");
const refLinks_1 = require("./refLinks");
const repository_1 = require("./repository");
const repositoryAllOf_1 = require("./repositoryAllOf");
const repositoryAllOfLinks_1 = require("./repositoryAllOfLinks");
const repositoryPermission_1 = require("./repositoryPermission");
const searchCodeSearchResult_1 = require("./searchCodeSearchResult");
const searchContentMatch_1 = require("./searchContentMatch");
const searchLine_1 = require("./searchLine");
const searchResultPage_1 = require("./searchResultPage");
const searchSegment_1 = require("./searchSegment");
const snippet_1 = require("./snippet");
const snippetAllOf_1 = require("./snippetAllOf");
const snippetComment_1 = require("./snippetComment");
const snippetCommentAllOf_1 = require("./snippetCommentAllOf");
const snippetCommit_1 = require("./snippetCommit");
const snippetCommitAllOf_1 = require("./snippetCommitAllOf");
const snippetCommitAllOfLinks_1 = require("./snippetCommitAllOfLinks");
const sshAccountKey_1 = require("./sshAccountKey");
const sshAccountKeyAllOf_1 = require("./sshAccountKeyAllOf");
const sshKey_1 = require("./sshKey");
const sshKeyAllOf_1 = require("./sshKeyAllOf");
const subjectTypes_1 = require("./subjectTypes");
const subjectTypesRepository_1 = require("./subjectTypesRepository");
const tag_1 = require("./tag");
const tagAllOf_1 = require("./tagAllOf");
const team_1 = require("./team");
const teamPermission_1 = require("./teamPermission");
const treeentry_1 = require("./treeentry");
const user_1 = require("./user");
const userAllOf_1 = require("./userAllOf");
const version_1 = require("./version");
const versionAllOf_1 = require("./versionAllOf");
const webhookSubscription_1 = require("./webhookSubscription");
const webhookSubscriptionAllOf_1 = require("./webhookSubscriptionAllOf");
/* tslint:disable:no-unused-variable */
let primitives = [
    "string",
    "boolean",
    "double",
    "integer",
    "long",
    "float",
    "number",
    "any"
];
let enumsMap = {
    "BranchingModelAllOfBranchTypes.KindEnum": branchingModelAllOfBranchTypes_1.BranchingModelAllOfBranchTypes.KindEnum,
    "BranchingModelSettingsAllOfBranchTypes.KindEnum": branchingModelSettingsAllOfBranchTypes_1.BranchingModelSettingsAllOfBranchTypes.KindEnum,
    "Branchrestriction.KindEnum": branchrestriction_1.Branchrestriction.KindEnum,
    "Branchrestriction.BranchMatchKindEnum": branchrestriction_1.Branchrestriction.BranchMatchKindEnum,
    "Branchrestriction.BranchTypeEnum": branchrestriction_1.Branchrestriction.BranchTypeEnum,
    "BranchrestrictionAllOf.KindEnum": branchrestrictionAllOf_1.BranchrestrictionAllOf.KindEnum,
    "BranchrestrictionAllOf.BranchMatchKindEnum": branchrestrictionAllOf_1.BranchrestrictionAllOf.BranchMatchKindEnum,
    "BranchrestrictionAllOf.BranchTypeEnum": branchrestrictionAllOf_1.BranchrestrictionAllOf.BranchTypeEnum,
    "CommentContent.MarkupEnum": commentContent_1.CommentContent.MarkupEnum,
    "CommitFile.AttributesEnum": commitFile_1.CommitFile.AttributesEnum,
    "Commitstatus.StateEnum": commitstatus_1.Commitstatus.StateEnum,
    "CommitstatusAllOf.StateEnum": commitstatusAllOf_1.CommitstatusAllOf.StateEnum,
    "DeploymentStateCompleted.NameEnum": deploymentStateCompleted_1.DeploymentStateCompleted.NameEnum,
    "DeploymentStateCompletedAllOf.NameEnum": deploymentStateCompletedAllOf_1.DeploymentStateCompletedAllOf.NameEnum,
    "DeploymentStateCompletedStatusFailed.NameEnum": deploymentStateCompletedStatusFailed_1.DeploymentStateCompletedStatusFailed.NameEnum,
    "DeploymentStateCompletedStatusFailedAllOf.NameEnum": deploymentStateCompletedStatusFailedAllOf_1.DeploymentStateCompletedStatusFailedAllOf.NameEnum,
    "DeploymentStateCompletedStatusStopped.NameEnum": deploymentStateCompletedStatusStopped_1.DeploymentStateCompletedStatusStopped.NameEnum,
    "DeploymentStateCompletedStatusStoppedAllOf.NameEnum": deploymentStateCompletedStatusStoppedAllOf_1.DeploymentStateCompletedStatusStoppedAllOf.NameEnum,
    "DeploymentStateCompletedStatusSuccessful.NameEnum": deploymentStateCompletedStatusSuccessful_1.DeploymentStateCompletedStatusSuccessful.NameEnum,
    "DeploymentStateCompletedStatusSuccessfulAllOf.NameEnum": deploymentStateCompletedStatusSuccessfulAllOf_1.DeploymentStateCompletedStatusSuccessfulAllOf.NameEnum,
    "DeploymentStateInProgress.NameEnum": deploymentStateInProgress_1.DeploymentStateInProgress.NameEnum,
    "DeploymentStateInProgressAllOf.NameEnum": deploymentStateInProgressAllOf_1.DeploymentStateInProgressAllOf.NameEnum,
    "DeploymentStateUndeployed.NameEnum": deploymentStateUndeployed_1.DeploymentStateUndeployed.NameEnum,
    "DeploymentStateUndeployedAllOf.NameEnum": deploymentStateUndeployedAllOf_1.DeploymentStateUndeployedAllOf.NameEnum,
    "Diffstat.StatusEnum": diffstat_1.Diffstat.StatusEnum,
    "HookEvent.EventEnum": hookEvent_1.HookEvent.EventEnum,
    "Issue.PriorityEnum": issue_1.Issue.PriorityEnum,
    "Issue.StateEnum": issue_1.Issue.StateEnum,
    "Issue.KindEnum": issue_1.Issue.KindEnum,
    "IssueAllOf.PriorityEnum": issueAllOf_1.IssueAllOf.PriorityEnum,
    "IssueAllOf.StateEnum": issueAllOf_1.IssueAllOf.StateEnum,
    "IssueAllOf.KindEnum": issueAllOf_1.IssueAllOf.KindEnum,
    "IssueAllOfContent.MarkupEnum": issueAllOfContent_1.IssueAllOfContent.MarkupEnum,
    "IssueChangeMessage.MarkupEnum": issueChangeMessage_1.IssueChangeMessage.MarkupEnum,
    "IssueJobStatus.StatusEnum": issueJobStatus_1.IssueJobStatus.StatusEnum,
    "Participant.RoleEnum": participant_1.Participant.RoleEnum,
    "ParticipantAllOf.RoleEnum": participantAllOf_1.ParticipantAllOf.RoleEnum,
    "PipelineRefTarget.RefTypeEnum": pipelineRefTarget_1.PipelineRefTarget.RefTypeEnum,
    "PipelineRefTargetAllOf.RefTypeEnum": pipelineRefTargetAllOf_1.PipelineRefTargetAllOf.RefTypeEnum,
    "PipelineStateCompleted.NameEnum": pipelineStateCompleted_1.PipelineStateCompleted.NameEnum,
    "PipelineStateCompletedAllOf.NameEnum": pipelineStateCompletedAllOf_1.PipelineStateCompletedAllOf.NameEnum,
    "PipelineStateCompletedError.NameEnum": pipelineStateCompletedError_1.PipelineStateCompletedError.NameEnum,
    "PipelineStateCompletedErrorAllOf.NameEnum": pipelineStateCompletedErrorAllOf_1.PipelineStateCompletedErrorAllOf.NameEnum,
    "PipelineStateCompletedExpired.NameEnum": pipelineStateCompletedExpired_1.PipelineStateCompletedExpired.NameEnum,
    "PipelineStateCompletedExpiredAllOf.NameEnum": pipelineStateCompletedExpiredAllOf_1.PipelineStateCompletedExpiredAllOf.NameEnum,
    "PipelineStateCompletedFailed.NameEnum": pipelineStateCompletedFailed_1.PipelineStateCompletedFailed.NameEnum,
    "PipelineStateCompletedFailedAllOf.NameEnum": pipelineStateCompletedFailedAllOf_1.PipelineStateCompletedFailedAllOf.NameEnum,
    "PipelineStateCompletedStopped.NameEnum": pipelineStateCompletedStopped_1.PipelineStateCompletedStopped.NameEnum,
    "PipelineStateCompletedStoppedAllOf.NameEnum": pipelineStateCompletedStoppedAllOf_1.PipelineStateCompletedStoppedAllOf.NameEnum,
    "PipelineStateCompletedSuccessful.NameEnum": pipelineStateCompletedSuccessful_1.PipelineStateCompletedSuccessful.NameEnum,
    "PipelineStateCompletedSuccessfulAllOf.NameEnum": pipelineStateCompletedSuccessfulAllOf_1.PipelineStateCompletedSuccessfulAllOf.NameEnum,
    "PipelineStateInProgress.NameEnum": pipelineStateInProgress_1.PipelineStateInProgress.NameEnum,
    "PipelineStateInProgressAllOf.NameEnum": pipelineStateInProgressAllOf_1.PipelineStateInProgressAllOf.NameEnum,
    "PipelineStateInProgressPaused.NameEnum": pipelineStateInProgressPaused_1.PipelineStateInProgressPaused.NameEnum,
    "PipelineStateInProgressPausedAllOf.NameEnum": pipelineStateInProgressPausedAllOf_1.PipelineStateInProgressPausedAllOf.NameEnum,
    "PipelineStateInProgressRunning.NameEnum": pipelineStateInProgressRunning_1.PipelineStateInProgressRunning.NameEnum,
    "PipelineStateInProgressRunningAllOf.NameEnum": pipelineStateInProgressRunningAllOf_1.PipelineStateInProgressRunningAllOf.NameEnum,
    "PipelineStatePending.NameEnum": pipelineStatePending_1.PipelineStatePending.NameEnum,
    "PipelineStatePendingAllOf.NameEnum": pipelineStatePendingAllOf_1.PipelineStatePendingAllOf.NameEnum,
    "PipelineStepStateCompleted.NameEnum": pipelineStepStateCompleted_1.PipelineStepStateCompleted.NameEnum,
    "PipelineStepStateCompletedAllOf.NameEnum": pipelineStepStateCompletedAllOf_1.PipelineStepStateCompletedAllOf.NameEnum,
    "PipelineStepStateCompletedError.NameEnum": pipelineStepStateCompletedError_1.PipelineStepStateCompletedError.NameEnum,
    "PipelineStepStateCompletedErrorAllOf.NameEnum": pipelineStepStateCompletedErrorAllOf_1.PipelineStepStateCompletedErrorAllOf.NameEnum,
    "PipelineStepStateCompletedExpired.NameEnum": pipelineStepStateCompletedExpired_1.PipelineStepStateCompletedExpired.NameEnum,
    "PipelineStepStateCompletedExpiredAllOf.NameEnum": pipelineStepStateCompletedExpiredAllOf_1.PipelineStepStateCompletedExpiredAllOf.NameEnum,
    "PipelineStepStateCompletedFailed.NameEnum": pipelineStepStateCompletedFailed_1.PipelineStepStateCompletedFailed.NameEnum,
    "PipelineStepStateCompletedFailedAllOf.NameEnum": pipelineStepStateCompletedFailedAllOf_1.PipelineStepStateCompletedFailedAllOf.NameEnum,
    "PipelineStepStateCompletedNotRun.NameEnum": pipelineStepStateCompletedNotRun_1.PipelineStepStateCompletedNotRun.NameEnum,
    "PipelineStepStateCompletedNotRunAllOf.NameEnum": pipelineStepStateCompletedNotRunAllOf_1.PipelineStepStateCompletedNotRunAllOf.NameEnum,
    "PipelineStepStateCompletedStopped.NameEnum": pipelineStepStateCompletedStopped_1.PipelineStepStateCompletedStopped.NameEnum,
    "PipelineStepStateCompletedStoppedAllOf.NameEnum": pipelineStepStateCompletedStoppedAllOf_1.PipelineStepStateCompletedStoppedAllOf.NameEnum,
    "PipelineStepStateCompletedSuccessful.NameEnum": pipelineStepStateCompletedSuccessful_1.PipelineStepStateCompletedSuccessful.NameEnum,
    "PipelineStepStateCompletedSuccessfulAllOf.NameEnum": pipelineStepStateCompletedSuccessfulAllOf_1.PipelineStepStateCompletedSuccessfulAllOf.NameEnum,
    "PipelineStepStateInProgress.NameEnum": pipelineStepStateInProgress_1.PipelineStepStateInProgress.NameEnum,
    "PipelineStepStateInProgressAllOf.NameEnum": pipelineStepStateInProgressAllOf_1.PipelineStepStateInProgressAllOf.NameEnum,
    "PipelineStepStatePending.NameEnum": pipelineStepStatePending_1.PipelineStepStatePending.NameEnum,
    "PipelineStepStatePendingAllOf.NameEnum": pipelineStepStatePendingAllOf_1.PipelineStepStatePendingAllOf.NameEnum,
    "PipelineStepStateReady.NameEnum": pipelineStepStateReady_1.PipelineStepStateReady.NameEnum,
    "PipelineStepStateReadyAllOf.NameEnum": pipelineStepStateReadyAllOf_1.PipelineStepStateReadyAllOf.NameEnum,
    "Pullrequest.StateEnum": pullrequest_1.Pullrequest.StateEnum,
    "PullrequestAllOf.StateEnum": pullrequestAllOf_1.PullrequestAllOf.StateEnum,
    "PullrequestEndpointBranch.MergeStrategiesEnum": pullrequestEndpointBranch_1.PullrequestEndpointBranch.MergeStrategiesEnum,
    "PullrequestMergeParameters.MergeStrategyEnum": pullrequestMergeParameters_1.PullrequestMergeParameters.MergeStrategyEnum,
    "Repository.ScmEnum": repository_1.Repository.ScmEnum,
    "Repository.ForkPolicyEnum": repository_1.Repository.ForkPolicyEnum,
    "RepositoryAllOf.ScmEnum": repositoryAllOf_1.RepositoryAllOf.ScmEnum,
    "RepositoryAllOf.ForkPolicyEnum": repositoryAllOf_1.RepositoryAllOf.ForkPolicyEnum,
    "RepositoryPermission.PermissionEnum": repositoryPermission_1.RepositoryPermission.PermissionEnum,
    "Snippet.ScmEnum": snippet_1.Snippet.ScmEnum,
    "SnippetAllOf.ScmEnum": snippetAllOf_1.SnippetAllOf.ScmEnum,
    "TeamPermission.PermissionEnum": teamPermission_1.TeamPermission.PermissionEnum,
    "WebhookSubscription.SubjectTypeEnum": webhookSubscription_1.WebhookSubscription.SubjectTypeEnum,
    "WebhookSubscription.EventsEnum": webhookSubscription_1.WebhookSubscription.EventsEnum,
    "WebhookSubscriptionAllOf.SubjectTypeEnum": webhookSubscriptionAllOf_1.WebhookSubscriptionAllOf.SubjectTypeEnum,
    "WebhookSubscriptionAllOf.EventsEnum": webhookSubscriptionAllOf_1.WebhookSubscriptionAllOf.EventsEnum,
};
let typeMap = {
    "Account": account_1.Account,
    "AccountAllOf": accountAllOf_1.AccountAllOf,
    "AccountAllOfLinks": accountAllOfLinks_1.AccountAllOfLinks,
    "Activity": activity_1.Activity,
    "Author": author_1.Author,
    "AuthorAllOf": authorAllOf_1.AuthorAllOf,
    "BaseCommit": baseCommit_1.BaseCommit,
    "BaseCommitAllOf": baseCommitAllOf_1.BaseCommitAllOf,
    "Branch": branch_1.Branch,
    "BranchingModel": branchingModel_1.BranchingModel,
    "BranchingModelAllOf": branchingModelAllOf_1.BranchingModelAllOf,
    "BranchingModelAllOfBranchTypes": branchingModelAllOfBranchTypes_1.BranchingModelAllOfBranchTypes,
    "BranchingModelAllOfProduction": branchingModelAllOfProduction_1.BranchingModelAllOfProduction,
    "BranchingModelSettings": branchingModelSettings_1.BranchingModelSettings,
    "BranchingModelSettingsAllOf": branchingModelSettingsAllOf_1.BranchingModelSettingsAllOf,
    "BranchingModelSettingsAllOfBranchTypes": branchingModelSettingsAllOfBranchTypes_1.BranchingModelSettingsAllOfBranchTypes,
    "BranchingModelSettingsAllOfDevelopment": branchingModelSettingsAllOfDevelopment_1.BranchingModelSettingsAllOfDevelopment,
    "BranchingModelSettingsAllOfLinks": branchingModelSettingsAllOfLinks_1.BranchingModelSettingsAllOfLinks,
    "BranchingModelSettingsAllOfLinksSelf": branchingModelSettingsAllOfLinksSelf_1.BranchingModelSettingsAllOfLinksSelf,
    "BranchingModelSettingsAllOfProduction": branchingModelSettingsAllOfProduction_1.BranchingModelSettingsAllOfProduction,
    "Branchrestriction": branchrestriction_1.Branchrestriction,
    "BranchrestrictionAllOf": branchrestrictionAllOf_1.BranchrestrictionAllOf,
    "Comment": comment_1.Comment,
    "CommentAllOf": commentAllOf_1.CommentAllOf,
    "CommentAllOfLinks": commentAllOfLinks_1.CommentAllOfLinks,
    "CommentContent": commentContent_1.CommentContent,
    "CommentInline": commentInline_1.CommentInline,
    "Commit": commit_1.Commit,
    "CommitAllOf": commitAllOf_1.CommitAllOf,
    "CommitComment": commitComment_1.CommitComment,
    "CommitCommentAllOf": commitCommentAllOf_1.CommitCommentAllOf,
    "CommitFile": commitFile_1.CommitFile,
    "Commitstatus": commitstatus_1.Commitstatus,
    "CommitstatusAllOf": commitstatusAllOf_1.CommitstatusAllOf,
    "CommitstatusAllOfLinks": commitstatusAllOfLinks_1.CommitstatusAllOfLinks,
    "Component": component_1.Component,
    "ComponentAllOf": componentAllOf_1.ComponentAllOf,
    "DeployKey": deployKey_1.DeployKey,
    "DeployKeyAllOf": deployKeyAllOf_1.DeployKeyAllOf,
    "Deployment": deployment_1.Deployment,
    "DeploymentAllOf": deploymentAllOf_1.DeploymentAllOf,
    "DeploymentEnvironment": deploymentEnvironment_1.DeploymentEnvironment,
    "DeploymentEnvironmentAllOf": deploymentEnvironmentAllOf_1.DeploymentEnvironmentAllOf,
    "DeploymentRelease": deploymentRelease_1.DeploymentRelease,
    "DeploymentReleaseAllOf": deploymentReleaseAllOf_1.DeploymentReleaseAllOf,
    "DeploymentState": deploymentState_1.DeploymentState,
    "DeploymentStateCompleted": deploymentStateCompleted_1.DeploymentStateCompleted,
    "DeploymentStateCompletedAllOf": deploymentStateCompletedAllOf_1.DeploymentStateCompletedAllOf,
    "DeploymentStateCompletedStatus": deploymentStateCompletedStatus_1.DeploymentStateCompletedStatus,
    "DeploymentStateCompletedStatusFailed": deploymentStateCompletedStatusFailed_1.DeploymentStateCompletedStatusFailed,
    "DeploymentStateCompletedStatusFailedAllOf": deploymentStateCompletedStatusFailedAllOf_1.DeploymentStateCompletedStatusFailedAllOf,
    "DeploymentStateCompletedStatusStopped": deploymentStateCompletedStatusStopped_1.DeploymentStateCompletedStatusStopped,
    "DeploymentStateCompletedStatusStoppedAllOf": deploymentStateCompletedStatusStoppedAllOf_1.DeploymentStateCompletedStatusStoppedAllOf,
    "DeploymentStateCompletedStatusSuccessful": deploymentStateCompletedStatusSuccessful_1.DeploymentStateCompletedStatusSuccessful,
    "DeploymentStateCompletedStatusSuccessfulAllOf": deploymentStateCompletedStatusSuccessfulAllOf_1.DeploymentStateCompletedStatusSuccessfulAllOf,
    "DeploymentStateInProgress": deploymentStateInProgress_1.DeploymentStateInProgress,
    "DeploymentStateInProgressAllOf": deploymentStateInProgressAllOf_1.DeploymentStateInProgressAllOf,
    "DeploymentStateUndeployed": deploymentStateUndeployed_1.DeploymentStateUndeployed,
    "DeploymentStateUndeployedAllOf": deploymentStateUndeployedAllOf_1.DeploymentStateUndeployedAllOf,
    "Diffstat": diffstat_1.Diffstat,
    "Error": error_1.Error,
    "ErrorError": errorError_1.ErrorError,
    "Group": group_1.Group,
    "GroupAllOf": groupAllOf_1.GroupAllOf,
    "GroupAllOfLinks": groupAllOfLinks_1.GroupAllOfLinks,
    "HookEvent": hookEvent_1.HookEvent,
    "Issue": issue_1.Issue,
    "IssueAllOf": issueAllOf_1.IssueAllOf,
    "IssueAllOfContent": issueAllOfContent_1.IssueAllOfContent,
    "IssueAllOfLinks": issueAllOfLinks_1.IssueAllOfLinks,
    "IssueAttachment": issueAttachment_1.IssueAttachment,
    "IssueAttachmentAllOf": issueAttachmentAllOf_1.IssueAttachmentAllOf,
    "IssueChange": issueChange_1.IssueChange,
    "IssueChangeChanges": issueChangeChanges_1.IssueChangeChanges,
    "IssueChangeChangesComponent": issueChangeChangesComponent_1.IssueChangeChangesComponent,
    "IssueChangeLinks": issueChangeLinks_1.IssueChangeLinks,
    "IssueChangeLinksIssue": issueChangeLinksIssue_1.IssueChangeLinksIssue,
    "IssueChangeMessage": issueChangeMessage_1.IssueChangeMessage,
    "IssueComment": issueComment_1.IssueComment,
    "IssueCommentAllOf": issueCommentAllOf_1.IssueCommentAllOf,
    "IssueJobStatus": issueJobStatus_1.IssueJobStatus,
    "Milestone": milestone_1.Milestone,
    "MilestoneAllOf": milestoneAllOf_1.MilestoneAllOf,
    "ModelObject": modelObject_1.ModelObject,
    "Page": page_1.Page,
    "PaginatedActivities": paginatedActivities_1.PaginatedActivities,
    "PaginatedBranches": paginatedBranches_1.PaginatedBranches,
    "PaginatedBranchrestrictions": paginatedBranchrestrictions_1.PaginatedBranchrestrictions,
    "PaginatedCommitComments": paginatedCommitComments_1.PaginatedCommitComments,
    "PaginatedCommitstatuses": paginatedCommitstatuses_1.PaginatedCommitstatuses,
    "PaginatedComponents": paginatedComponents_1.PaginatedComponents,
    "PaginatedDeployKeys": paginatedDeployKeys_1.PaginatedDeployKeys,
    "PaginatedDeployments": paginatedDeployments_1.PaginatedDeployments,
    "PaginatedDiffstats": paginatedDiffstats_1.PaginatedDiffstats,
    "PaginatedEnvironments": paginatedEnvironments_1.PaginatedEnvironments,
    "PaginatedFiles": paginatedFiles_1.PaginatedFiles,
    "PaginatedHookEvents": paginatedHookEvents_1.PaginatedHookEvents,
    "PaginatedIssueAttachments": paginatedIssueAttachments_1.PaginatedIssueAttachments,
    "PaginatedIssueComments": paginatedIssueComments_1.PaginatedIssueComments,
    "PaginatedIssues": paginatedIssues_1.PaginatedIssues,
    "PaginatedLogEntries": paginatedLogEntries_1.PaginatedLogEntries,
    "PaginatedMilestones": paginatedMilestones_1.PaginatedMilestones,
    "PaginatedPipelineKnownHosts": paginatedPipelineKnownHosts_1.PaginatedPipelineKnownHosts,
    "PaginatedPipelineScheduleExecutions": paginatedPipelineScheduleExecutions_1.PaginatedPipelineScheduleExecutions,
    "PaginatedPipelineSchedules": paginatedPipelineSchedules_1.PaginatedPipelineSchedules,
    "PaginatedPipelineSteps": paginatedPipelineSteps_1.PaginatedPipelineSteps,
    "PaginatedPipelineVariables": paginatedPipelineVariables_1.PaginatedPipelineVariables,
    "PaginatedPipelines": paginatedPipelines_1.PaginatedPipelines,
    "PaginatedProjects": paginatedProjects_1.PaginatedProjects,
    "PaginatedPullrequestComments": paginatedPullrequestComments_1.PaginatedPullrequestComments,
    "PaginatedPullrequests": paginatedPullrequests_1.PaginatedPullrequests,
    "PaginatedPullrequestsCommits": paginatedPullrequestsCommits_1.PaginatedPullrequestsCommits,
    "PaginatedRefs": paginatedRefs_1.PaginatedRefs,
    "PaginatedRepositories": paginatedRepositories_1.PaginatedRepositories,
    "PaginatedRepositoryPermissions": paginatedRepositoryPermissions_1.PaginatedRepositoryPermissions,
    "PaginatedSnippetComments": paginatedSnippetComments_1.PaginatedSnippetComments,
    "PaginatedSnippetCommit": paginatedSnippetCommit_1.PaginatedSnippetCommit,
    "PaginatedSnippets": paginatedSnippets_1.PaginatedSnippets,
    "PaginatedSshUserKeys": paginatedSshUserKeys_1.PaginatedSshUserKeys,
    "PaginatedTags": paginatedTags_1.PaginatedTags,
    "PaginatedTeamPermissions": paginatedTeamPermissions_1.PaginatedTeamPermissions,
    "PaginatedTeams": paginatedTeams_1.PaginatedTeams,
    "PaginatedTreeentries": paginatedTreeentries_1.PaginatedTreeentries,
    "PaginatedUsers": paginatedUsers_1.PaginatedUsers,
    "PaginatedVersions": paginatedVersions_1.PaginatedVersions,
    "PaginatedWebhookSubscriptions": paginatedWebhookSubscriptions_1.PaginatedWebhookSubscriptions,
    "Participant": participant_1.Participant,
    "ParticipantAllOf": participantAllOf_1.ParticipantAllOf,
    "Pipeline": pipeline_1.Pipeline,
    "PipelineAllOf": pipelineAllOf_1.PipelineAllOf,
    "PipelineBuildNumber": pipelineBuildNumber_1.PipelineBuildNumber,
    "PipelineBuildNumberAllOf": pipelineBuildNumberAllOf_1.PipelineBuildNumberAllOf,
    "PipelineCommand": pipelineCommand_1.PipelineCommand,
    "PipelineCommitTarget": pipelineCommitTarget_1.PipelineCommitTarget,
    "PipelineCommitTargetAllOf": pipelineCommitTargetAllOf_1.PipelineCommitTargetAllOf,
    "PipelineError": pipelineError_1.PipelineError,
    "PipelineErrorAllOf": pipelineErrorAllOf_1.PipelineErrorAllOf,
    "PipelineImage": pipelineImage_1.PipelineImage,
    "PipelineKnownHost": pipelineKnownHost_1.PipelineKnownHost,
    "PipelineKnownHostAllOf": pipelineKnownHostAllOf_1.PipelineKnownHostAllOf,
    "PipelineRefTarget": pipelineRefTarget_1.PipelineRefTarget,
    "PipelineRefTargetAllOf": pipelineRefTargetAllOf_1.PipelineRefTargetAllOf,
    "PipelineSchedule": pipelineSchedule_1.PipelineSchedule,
    "PipelineScheduleAllOf": pipelineScheduleAllOf_1.PipelineScheduleAllOf,
    "PipelineScheduleExecution": pipelineScheduleExecution_1.PipelineScheduleExecution,
    "PipelineScheduleExecutionErrored": pipelineScheduleExecutionErrored_1.PipelineScheduleExecutionErrored,
    "PipelineScheduleExecutionErroredAllOf": pipelineScheduleExecutionErroredAllOf_1.PipelineScheduleExecutionErroredAllOf,
    "PipelineScheduleExecutionExecuted": pipelineScheduleExecutionExecuted_1.PipelineScheduleExecutionExecuted,
    "PipelineScheduleExecutionExecutedAllOf": pipelineScheduleExecutionExecutedAllOf_1.PipelineScheduleExecutionExecutedAllOf,
    "PipelineSelector": pipelineSelector_1.PipelineSelector,
    "PipelineSshKeyPair": pipelineSshKeyPair_1.PipelineSshKeyPair,
    "PipelineSshKeyPairAllOf": pipelineSshKeyPairAllOf_1.PipelineSshKeyPairAllOf,
    "PipelineSshPublicKey": pipelineSshPublicKey_1.PipelineSshPublicKey,
    "PipelineSshPublicKeyAllOf": pipelineSshPublicKeyAllOf_1.PipelineSshPublicKeyAllOf,
    "PipelineState": pipelineState_1.PipelineState,
    "PipelineStateCompleted": pipelineStateCompleted_1.PipelineStateCompleted,
    "PipelineStateCompletedAllOf": pipelineStateCompletedAllOf_1.PipelineStateCompletedAllOf,
    "PipelineStateCompletedError": pipelineStateCompletedError_1.PipelineStateCompletedError,
    "PipelineStateCompletedErrorAllOf": pipelineStateCompletedErrorAllOf_1.PipelineStateCompletedErrorAllOf,
    "PipelineStateCompletedExpired": pipelineStateCompletedExpired_1.PipelineStateCompletedExpired,
    "PipelineStateCompletedExpiredAllOf": pipelineStateCompletedExpiredAllOf_1.PipelineStateCompletedExpiredAllOf,
    "PipelineStateCompletedFailed": pipelineStateCompletedFailed_1.PipelineStateCompletedFailed,
    "PipelineStateCompletedFailedAllOf": pipelineStateCompletedFailedAllOf_1.PipelineStateCompletedFailedAllOf,
    "PipelineStateCompletedResult": pipelineStateCompletedResult_1.PipelineStateCompletedResult,
    "PipelineStateCompletedStopped": pipelineStateCompletedStopped_1.PipelineStateCompletedStopped,
    "PipelineStateCompletedStoppedAllOf": pipelineStateCompletedStoppedAllOf_1.PipelineStateCompletedStoppedAllOf,
    "PipelineStateCompletedSuccessful": pipelineStateCompletedSuccessful_1.PipelineStateCompletedSuccessful,
    "PipelineStateCompletedSuccessfulAllOf": pipelineStateCompletedSuccessfulAllOf_1.PipelineStateCompletedSuccessfulAllOf,
    "PipelineStateInProgress": pipelineStateInProgress_1.PipelineStateInProgress,
    "PipelineStateInProgressAllOf": pipelineStateInProgressAllOf_1.PipelineStateInProgressAllOf,
    "PipelineStateInProgressPaused": pipelineStateInProgressPaused_1.PipelineStateInProgressPaused,
    "PipelineStateInProgressPausedAllOf": pipelineStateInProgressPausedAllOf_1.PipelineStateInProgressPausedAllOf,
    "PipelineStateInProgressRunning": pipelineStateInProgressRunning_1.PipelineStateInProgressRunning,
    "PipelineStateInProgressRunningAllOf": pipelineStateInProgressRunningAllOf_1.PipelineStateInProgressRunningAllOf,
    "PipelineStateInProgressStage": pipelineStateInProgressStage_1.PipelineStateInProgressStage,
    "PipelineStatePending": pipelineStatePending_1.PipelineStatePending,
    "PipelineStatePendingAllOf": pipelineStatePendingAllOf_1.PipelineStatePendingAllOf,
    "PipelineStep": pipelineStep_1.PipelineStep,
    "PipelineStepAllOf": pipelineStepAllOf_1.PipelineStepAllOf,
    "PipelineStepError": pipelineStepError_1.PipelineStepError,
    "PipelineStepErrorAllOf": pipelineStepErrorAllOf_1.PipelineStepErrorAllOf,
    "PipelineStepState": pipelineStepState_1.PipelineStepState,
    "PipelineStepStateCompleted": pipelineStepStateCompleted_1.PipelineStepStateCompleted,
    "PipelineStepStateCompletedAllOf": pipelineStepStateCompletedAllOf_1.PipelineStepStateCompletedAllOf,
    "PipelineStepStateCompletedError": pipelineStepStateCompletedError_1.PipelineStepStateCompletedError,
    "PipelineStepStateCompletedErrorAllOf": pipelineStepStateCompletedErrorAllOf_1.PipelineStepStateCompletedErrorAllOf,
    "PipelineStepStateCompletedExpired": pipelineStepStateCompletedExpired_1.PipelineStepStateCompletedExpired,
    "PipelineStepStateCompletedExpiredAllOf": pipelineStepStateCompletedExpiredAllOf_1.PipelineStepStateCompletedExpiredAllOf,
    "PipelineStepStateCompletedFailed": pipelineStepStateCompletedFailed_1.PipelineStepStateCompletedFailed,
    "PipelineStepStateCompletedFailedAllOf": pipelineStepStateCompletedFailedAllOf_1.PipelineStepStateCompletedFailedAllOf,
    "PipelineStepStateCompletedNotRun": pipelineStepStateCompletedNotRun_1.PipelineStepStateCompletedNotRun,
    "PipelineStepStateCompletedNotRunAllOf": pipelineStepStateCompletedNotRunAllOf_1.PipelineStepStateCompletedNotRunAllOf,
    "PipelineStepStateCompletedResult": pipelineStepStateCompletedResult_1.PipelineStepStateCompletedResult,
    "PipelineStepStateCompletedStopped": pipelineStepStateCompletedStopped_1.PipelineStepStateCompletedStopped,
    "PipelineStepStateCompletedStoppedAllOf": pipelineStepStateCompletedStoppedAllOf_1.PipelineStepStateCompletedStoppedAllOf,
    "PipelineStepStateCompletedSuccessful": pipelineStepStateCompletedSuccessful_1.PipelineStepStateCompletedSuccessful,
    "PipelineStepStateCompletedSuccessfulAllOf": pipelineStepStateCompletedSuccessfulAllOf_1.PipelineStepStateCompletedSuccessfulAllOf,
    "PipelineStepStateInProgress": pipelineStepStateInProgress_1.PipelineStepStateInProgress,
    "PipelineStepStateInProgressAllOf": pipelineStepStateInProgressAllOf_1.PipelineStepStateInProgressAllOf,
    "PipelineStepStatePending": pipelineStepStatePending_1.PipelineStepStatePending,
    "PipelineStepStatePendingAllOf": pipelineStepStatePendingAllOf_1.PipelineStepStatePendingAllOf,
    "PipelineStepStateReady": pipelineStepStateReady_1.PipelineStepStateReady,
    "PipelineStepStateReadyAllOf": pipelineStepStateReadyAllOf_1.PipelineStepStateReadyAllOf,
    "PipelineTarget": pipelineTarget_1.PipelineTarget,
    "PipelineTrigger": pipelineTrigger_1.PipelineTrigger,
    "PipelineTriggerManual": pipelineTriggerManual_1.PipelineTriggerManual,
    "PipelineTriggerPush": pipelineTriggerPush_1.PipelineTriggerPush,
    "PipelineVariable": pipelineVariable_1.PipelineVariable,
    "PipelineVariableAllOf": pipelineVariableAllOf_1.PipelineVariableAllOf,
    "PipelinesConfig": pipelinesConfig_1.PipelinesConfig,
    "PipelinesConfigAllOf": pipelinesConfigAllOf_1.PipelinesConfigAllOf,
    "PipelinesDdevPipelineStep": pipelinesDdevPipelineStep_1.PipelinesDdevPipelineStep,
    "PipelinesStgWestPipelineStep": pipelinesStgWestPipelineStep_1.PipelinesStgWestPipelineStep,
    "Project": project_1.Project,
    "ProjectAllOf": projectAllOf_1.ProjectAllOf,
    "ProjectAllOfLinks": projectAllOfLinks_1.ProjectAllOfLinks,
    "Pullrequest": pullrequest_1.Pullrequest,
    "PullrequestAllOf": pullrequestAllOf_1.PullrequestAllOf,
    "PullrequestAllOfLinks": pullrequestAllOfLinks_1.PullrequestAllOfLinks,
    "PullrequestAllOfMergeCommit": pullrequestAllOfMergeCommit_1.PullrequestAllOfMergeCommit,
    "PullrequestAllOfRendered": pullrequestAllOfRendered_1.PullrequestAllOfRendered,
    "PullrequestEndpoint": pullrequestEndpoint_1.PullrequestEndpoint,
    "PullrequestEndpointBranch": pullrequestEndpointBranch_1.PullrequestEndpointBranch,
    "PullrequestEndpointCommit": pullrequestEndpointCommit_1.PullrequestEndpointCommit,
    "PullrequestMergeParameters": pullrequestMergeParameters_1.PullrequestMergeParameters,
    "Ref": ref_1.Ref,
    "RefLinks": refLinks_1.RefLinks,
    "Repository": repository_1.Repository,
    "RepositoryAllOf": repositoryAllOf_1.RepositoryAllOf,
    "RepositoryAllOfLinks": repositoryAllOfLinks_1.RepositoryAllOfLinks,
    "RepositoryPermission": repositoryPermission_1.RepositoryPermission,
    "SearchCodeSearchResult": searchCodeSearchResult_1.SearchCodeSearchResult,
    "SearchContentMatch": searchContentMatch_1.SearchContentMatch,
    "SearchLine": searchLine_1.SearchLine,
    "SearchResultPage": searchResultPage_1.SearchResultPage,
    "SearchSegment": searchSegment_1.SearchSegment,
    "Snippet": snippet_1.Snippet,
    "SnippetAllOf": snippetAllOf_1.SnippetAllOf,
    "SnippetComment": snippetComment_1.SnippetComment,
    "SnippetCommentAllOf": snippetCommentAllOf_1.SnippetCommentAllOf,
    "SnippetCommit": snippetCommit_1.SnippetCommit,
    "SnippetCommitAllOf": snippetCommitAllOf_1.SnippetCommitAllOf,
    "SnippetCommitAllOfLinks": snippetCommitAllOfLinks_1.SnippetCommitAllOfLinks,
    "SshAccountKey": sshAccountKey_1.SshAccountKey,
    "SshAccountKeyAllOf": sshAccountKeyAllOf_1.SshAccountKeyAllOf,
    "SshKey": sshKey_1.SshKey,
    "SshKeyAllOf": sshKeyAllOf_1.SshKeyAllOf,
    "SubjectTypes": subjectTypes_1.SubjectTypes,
    "SubjectTypesRepository": subjectTypesRepository_1.SubjectTypesRepository,
    "Tag": tag_1.Tag,
    "TagAllOf": tagAllOf_1.TagAllOf,
    "Team": team_1.Team,
    "TeamPermission": teamPermission_1.TeamPermission,
    "Treeentry": treeentry_1.Treeentry,
    "User": user_1.User,
    "UserAllOf": userAllOf_1.UserAllOf,
    "Version": version_1.Version,
    "VersionAllOf": versionAllOf_1.VersionAllOf,
    "WebhookSubscription": webhookSubscription_1.WebhookSubscription,
    "WebhookSubscriptionAllOf": webhookSubscriptionAllOf_1.WebhookSubscriptionAllOf,
};
class ObjectSerializer {
    static findCorrectType(data, expectedType) {
        if (data == undefined) {
            return expectedType;
        }
        else if (primitives.indexOf(expectedType.toLowerCase()) !== -1) {
            return expectedType;
        }
        else if (expectedType === "Date") {
            return expectedType;
        }
        else {
            if (enumsMap[expectedType]) {
                return expectedType;
            }
            if (!typeMap[expectedType]) {
                return expectedType; // w/e we don't know the type
            }
            // Check the discriminator
            let discriminatorProperty = typeMap[expectedType].discriminator;
            if (discriminatorProperty == null) {
                return expectedType; // the type does not have a discriminator. use it.
            }
            else {
                if (data[discriminatorProperty]) {
                    var discriminatorType = data[discriminatorProperty];
                    if (typeMap[discriminatorType]) {
                        return discriminatorType; // use the type given in the discriminator
                    }
                    else {
                        return expectedType; // discriminator did not map to a type
                    }
                }
                else {
                    return expectedType; // discriminator was not present (or an empty string)
                }
            }
        }
    }
    static serialize(data, type) {
        if (data == undefined) {
            return data;
        }
        else if (primitives.indexOf(type.toLowerCase()) !== -1) {
            return data;
        }
        else if (type.lastIndexOf("Array<", 0) === 0) { // string.startsWith pre es6
            let subType = type.replace("Array<", ""); // Array<Type> => Type>
            subType = subType.substring(0, subType.length - 1); // Type> => Type
            let transformedData = [];
            for (let index in data) {
                let date = data[index];
                transformedData.push(ObjectSerializer.serialize(date, subType));
            }
            return transformedData;
        }
        else if (type === "Date") {
            return data.toISOString();
        }
        else {
            if (enumsMap[type]) {
                return data;
            }
            if (!typeMap[type]) { // in case we dont know the type
                return data;
            }
            // Get the actual type of this object
            type = this.findCorrectType(data, type);
            // get the map for the correct type.
            let attributeTypes = typeMap[type].getAttributeTypeMap();
            let instance = {};
            for (let index in attributeTypes) {
                let attributeType = attributeTypes[index];
                instance[attributeType.baseName] = ObjectSerializer.serialize(data[attributeType.name], attributeType.type);
            }
            return instance;
        }
    }
    static deserialize(data, type) {
        // polymorphism may change the actual type.
        type = ObjectSerializer.findCorrectType(data, type);
        if (data == undefined) {
            return data;
        }
        else if (primitives.indexOf(type.toLowerCase()) !== -1) {
            return data;
        }
        else if (type.lastIndexOf("Array<", 0) === 0) { // string.startsWith pre es6
            let subType = type.replace("Array<", ""); // Array<Type> => Type>
            subType = subType.substring(0, subType.length - 1); // Type> => Type
            let transformedData = [];
            for (let index in data) {
                let date = data[index];
                transformedData.push(ObjectSerializer.deserialize(date, subType));
            }
            return transformedData;
        }
        else if (type === "Date") {
            return new Date(data);
        }
        else {
            if (enumsMap[type]) { // is Enum
                return data;
            }
            if (!typeMap[type]) { // dont know the type
                return data;
            }
            let instance = new typeMap[type]();
            let attributeTypes = typeMap[type].getAttributeTypeMap();
            for (let index in attributeTypes) {
                let attributeType = attributeTypes[index];
                instance[attributeType.name] = ObjectSerializer.deserialize(data[attributeType.baseName], attributeType.type);
            }
            return instance;
        }
    }
}
exports.ObjectSerializer = ObjectSerializer;
class HttpBasicAuth {
    constructor() {
        this.username = '';
        this.password = '';
    }
    applyToRequest(requestOptions) {
        requestOptions.auth = {
            username: this.username, password: this.password
        };
    }
}
exports.HttpBasicAuth = HttpBasicAuth;
class ApiKeyAuth {
    constructor(location, paramName) {
        this.location = location;
        this.paramName = paramName;
        this.apiKey = '';
    }
    applyToRequest(requestOptions) {
        if (this.location == "query") {
            requestOptions.qs[this.paramName] = this.apiKey;
        }
        else if (this.location == "header" && requestOptions && requestOptions.headers) {
            requestOptions.headers[this.paramName] = this.apiKey;
        }
        else if (this.location == 'cookie' && requestOptions && requestOptions.headers) {
            if (requestOptions.headers['Cookie']) {
                requestOptions.headers['Cookie'] += '; ' + this.paramName + '=' + encodeURIComponent(this.apiKey);
            }
            else {
                requestOptions.headers['Cookie'] = this.paramName + '=' + encodeURIComponent(this.apiKey);
            }
        }
    }
}
exports.ApiKeyAuth = ApiKeyAuth;
class OAuth {
    constructor() {
        this.accessToken = '';
    }
    applyToRequest(requestOptions) {
        if (requestOptions && requestOptions.headers) {
            requestOptions.headers["Authorization"] = "Bearer " + this.accessToken;
        }
    }
}
exports.OAuth = OAuth;
class VoidAuth {
    constructor() {
        this.username = '';
        this.password = '';
    }
    applyToRequest(_) {
        // Do nothing
    }
}
exports.VoidAuth = VoidAuth;
