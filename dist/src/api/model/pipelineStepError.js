"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const modelObject_1 = require("./modelObject");
class PipelineStepError extends modelObject_1.ModelObject {
    static getAttributeTypeMap() {
        return super.getAttributeTypeMap().concat(PipelineStepError.attributeTypeMap);
    }
}
exports.PipelineStepError = PipelineStepError;
PipelineStepError.discriminator = undefined;
PipelineStepError.attributeTypeMap = [
    {
        "name": "message",
        "baseName": "message",
        "type": "string"
    },
    {
        "name": "key",
        "baseName": "key",
        "type": "string"
    }
];
