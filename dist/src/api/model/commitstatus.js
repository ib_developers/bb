"use strict";
/**
 * Bitbucket API
 * Code against the Bitbucket API to automate simple tasks, embed Bitbucket data into your own site, build mobile or desktop apps, or even add custom UI add-ons into Bitbucket itself using the Connect framework.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: support@bitbucket.org
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const modelObject_1 = require("./modelObject");
class Commitstatus extends modelObject_1.ModelObject {
    static getAttributeTypeMap() {
        return super.getAttributeTypeMap().concat(Commitstatus.attributeTypeMap);
    }
}
exports.Commitstatus = Commitstatus;
Commitstatus.discriminator = undefined;
Commitstatus.attributeTypeMap = [
    {
        "name": "description",
        "baseName": "description",
        "type": "string"
    },
    {
        "name": "updated_on",
        "baseName": "updated_on",
        "type": "Date"
    },
    {
        "name": "refname",
        "baseName": "refname",
        "type": "string"
    },
    {
        "name": "key",
        "baseName": "key",
        "type": "string"
    },
    {
        "name": "created_on",
        "baseName": "created_on",
        "type": "Date"
    },
    {
        "name": "url",
        "baseName": "url",
        "type": "string"
    },
    {
        "name": "name",
        "baseName": "name",
        "type": "string"
    },
    {
        "name": "links",
        "baseName": "links",
        "type": "CommitstatusAllOfLinks"
    },
    {
        "name": "uuid",
        "baseName": "uuid",
        "type": "string"
    },
    {
        "name": "state",
        "baseName": "state",
        "type": "Commitstatus.StateEnum"
    }
];
(function (Commitstatus) {
    let StateEnum;
    (function (StateEnum) {
        StateEnum[StateEnum["SUCCESSFUL"] = 'SUCCESSFUL'] = "SUCCESSFUL";
        StateEnum[StateEnum["FAILED"] = 'FAILED'] = "FAILED";
        StateEnum[StateEnum["INPROGRESS"] = 'INPROGRESS'] = "INPROGRESS";
        StateEnum[StateEnum["STOPPED"] = 'STOPPED'] = "STOPPED";
    })(StateEnum = Commitstatus.StateEnum || (Commitstatus.StateEnum = {}));
})(Commitstatus = exports.Commitstatus || (exports.Commitstatus = {}));
