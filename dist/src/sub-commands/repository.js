#!/usr/bin/env node
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const commander_1 = require("commander");
const url_1 = __importDefault(require("url"));
const fs_1 = require("fs");
const js_yaml_1 = __importDefault(require("js-yaml"));
const request_promise_1 = __importDefault(require("request-promise"));
const parse_1 = require("../parse");
const auth_1 = __importDefault(require("../auth"));
const withCatch_1 = __importDefault(require("../withCatch"));
const get_1 = __importDefault(require("../get"));
const repository = (_inStream, outStream) => {
    const program = parse_1.withMember(new commander_1.Command()
        .name('bb repository')
        .arguments('<username> <repo_slug>')
        .option('--get [file]')
        .option('--put <file>')
        .option('--delete'));
    program.action((username, repoSlug) => __awaiter(void 0, void 0, void 0, function* () {
        let request = {
            uri: url_1.default.format({
                protocol: 'https',
                hostname: 'api.bitbucket.org',
                pathname: `/2.0/repositories/${username}/${repoSlug}`
            }),
            json: true,
            auth: yield auth_1.default()
        };
        if (program.get) {
            get_1.default(program.get, yield withCatch_1.default(request_promise_1.default(request)), outStream);
            program.emit('end');
            return;
        }
        if (program.put) {
            const json = JSON.parse(fs_1.readFileSync(program.put, { encoding: 'utf-8' }));
            request = Object.assign(Object.assign({}, request), { method: 'PUT', json });
            yield withCatch_1.default(request_promise_1.default(request));
            program.emit('end');
            return;
        }
        if (program.delete) {
            request = Object.assign(Object.assign({}, request), { method: 'DELETE' });
            yield withCatch_1.default(request_promise_1.default(request));
            program.emit('end');
            return;
        }
        outStream.write(js_yaml_1.default.dump(parse_1.parse(yield withCatch_1.default(request_promise_1.default(request)), 'Repository', program.member || null)));
        program.emit('end');
    }));
    return program;
};
if (process.env.NODE_ENV !== 'test') {
    repository(process.stdin, process.stdout).parse(process.argv);
}
exports.default = repository;
