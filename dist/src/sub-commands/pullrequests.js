#!/usr/bin/env node
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const commander_1 = require("commander");
const safe_1 = __importDefault(require("colors/safe"));
const js_yaml_1 = __importDefault(require("js-yaml"));
const lodash_1 = require("lodash");
const url_1 = __importDefault(require("url"));
const request_promise_1 = __importDefault(require("request-promise"));
const auth_1 = __importDefault(require("../auth"));
const withCatch_1 = __importDefault(require("../withCatch"));
const parse_1 = require("../parse");
const get_1 = __importDefault(require("../get"));
const pullrequests = (_inStream, outStream) => {
    const program = parse_1.withMember(new commander_1.Command()
        .name('bb pullrequests')
        .arguments('<username> <repo_slug>')
        .option('--get [file]')
        .option('-p, --page <page>')
        .option('-q, --query <query>')
        .option('-s, --sort <sort>'));
    program.action((username, repoSlug) => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield withCatch_1.default(request_promise_1.default({
            uri: url_1.default.format({
                protocol: 'https',
                hostname: 'api.bitbucket.org',
                pathname: `2.0/repositories/${username}/${repoSlug}/pullrequests`,
                query: lodash_1.pickBy({
                    page: program.page,
                    q: program.query,
                    sort: program.sort
                })
            }),
            json: true,
            auth: yield auth_1.default()
        }));
        if (program.get) {
            get_1.default(program.get, response, outStream);
            program.emit('end');
            return;
        }
        if (response.values) {
            response.values.forEach(pullrequest => {
                outStream.write(safe_1.default.yellow(`#${pullrequest.id || ''} ${pullrequest.title || ''}\n`));
                outStream.write(js_yaml_1.default.dump(parse_1.parse(pullrequest, 'Pullrequest', program.member), program.member));
                outStream.write('\n');
            });
        }
        if (response.next) {
            const next = url_1.default.parse(response.next, true);
            outStream.write(`To get next, run with "-p ${next.query.page}". \n`);
        }
        program.emit('end');
    }));
    return program;
};
if (process.env.NODE_ENV !== 'test') {
    pullrequests(process.stdin, process.stdout).parse(process.argv);
}
exports.default = pullrequests;
