"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("request-promise/errors");
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const js_yaml_1 = __importDefault(require("js-yaml"));
const withCatch = (rp) => rp.catch(errors_1.StatusCodeError, ({ statusCode, error }) => {
    console.error(`${statusCode}: ${http_status_codes_1.default.getStatusText(statusCode)}`);
    const response = error;
    if (response) {
        console.error(js_yaml_1.default.dump(response.error));
    }
    process.exit(1);
});
exports.default = withCatch;
