"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const prompts_1 = __importDefault(require("prompts"));
const lodash_1 = require("lodash");
const conf_1 = __importDefault(require("./conf"));
const auth = () => __awaiter(void 0, void 0, void 0, function* () {
    if (process.env.NODE_ENV === 'test') {
        if (!conf_1.default.test) {
            throw new Error();
        }
        return conf_1.default.test.auth;
    }
    let authOptions = Object.assign({}, conf_1.default.auth);
    const response = yield prompts_1.default([
        {
            type: !authOptions.username && 'text',
            name: 'username',
            message: 'Username for Bitbucket Cloud'
        },
        {
            type: (_, { username }) => (username || authOptions.username) && !authOptions.password && 'password',
            name: 'password',
            message: (_, { username }) => `Password for ${username || authOptions.username}`
        }
    ]);
    authOptions = Object.assign(Object.assign({}, authOptions), lodash_1.pickBy(response));
    if (!authOptions.password) {
        delete authOptions.username;
    }
    return lodash_1.isEmpty(authOptions) ? undefined : authOptions;
});
exports.default = auth;
