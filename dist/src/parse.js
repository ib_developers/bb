"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const lodash_1 = require("lodash");
const parsers_1 = __importDefault(require("./parsers"));
const parse = (data, type, member) => {
    if (data === undefined || data === null) {
        return undefined;
    }
    const parser = parsers_1.default[type];
    if (parser) {
        let object = lodash_1.omitBy(parser.map.reduce((accumulator, { name, type: elementType }) => (Object.assign(Object.assign({}, accumulator), { [name]: parse(data[name], elementType) })), {}), lodash_1.isUndefined);
        if (member !== null) {
            object = lodash_1.pick(object, member || parser.default);
        }
        return Object.keys(object).length !== 0 ? object : undefined;
    }
    if (type.startsWith('Array<')) {
        const elementType = type.slice('Array<'.length, -'>'.length);
        return data.map((element) => parse(element, elementType));
    }
    if (type === 'Date') {
        return moment_1.default(data).fromNow();
    }
    if (type === 'string' || type === 'number' || type === 'boolean' || type.endsWith('Enum')) {
        return data;
    }
    throw new Error(`Unsupported parse type: ${type}`);
};
exports.parse = parse;
const withMember = (command) => {
    let count = 0;
    return command.option('-m, --member <member>', undefined, (value, previous) => {
        const next = (count ? previous : []).concat([value]);
        count += 1;
        return next;
    });
};
exports.withMember = withMember;
