"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("./api/model/models");
const parsers = {
    Account: {
        map: models_1.Account.getAttributeTypeMap(),
        default: ['display_name', 'website', 'has_2fa_enabled', 'account_status']
    },
    AccountAllOfLinks: {
        map: models_1.AccountAllOfLinks.getAttributeTypeMap(),
        default: []
    },
    Activity: {
        map: models_1.Activity.getAttributeTypeMap().concat([
            {
                name: 'approval',
                baseName: 'approval',
                type: 'Approval'
            },
            {
                name: 'pull_request',
                baseName: 'pull_request',
                type: 'Pullrequest'
            },
            {
                name: 'update',
                baseName: 'update',
                type: 'Update'
            }
        ]),
        default: ['approval', 'comment', 'pull_request', 'update']
    },
    Approval: {
        map: [
            {
                name: 'date',
                baseName: 'date',
                type: 'Date'
            },
            {
                name: 'pullrequest',
                baseName: 'pullrequest',
                type: 'Pullrequest'
            },
            {
                name: 'user',
                baseName: 'user',
                type: 'User'
            }
        ],
        default: ['date', 'pullrequest', 'user']
    },
    Branch: {
        map: models_1.Branch.getAttributeTypeMap(),
        default: ['name', 'target']
    },
    BranchingModelSettingsAllOfLinksSelf: {
        map: models_1.BranchingModelSettingsAllOfLinksSelf.getAttributeTypeMap(),
        default: []
    },
    Comment: {
        map: models_1.Comment.getAttributeTypeMap(),
        default: ['id', 'created_on', 'updated_on', 'deleted', 'user', 'inline', 'parent', 'content']
    },
    CommentAllOfLinks: {
        map: models_1.CommentAllOfLinks.getAttributeTypeMap(),
        default: []
    },
    CommentContent: {
        map: models_1.CommentContent.getAttributeTypeMap(),
        default: ['raw']
    },
    CommentInline: {
        map: models_1.CommentInline.getAttributeTypeMap(),
        default: ['path', 'from', 'to']
    },
    Issue: {
        map: models_1.Issue.getAttributeTypeMap(),
        default: ['priority', 'kind', 'reporter', 'created_on', 'updated_on', 'content']
    },
    IssueAllOfLinks: {
        map: models_1.IssueAllOfLinks.getAttributeTypeMap(),
        default: []
    },
    IssueAllOfContent: {
        map: models_1.IssueAllOfContent.getAttributeTypeMap(),
        default: ['raw']
    },
    Pullrequest: {
        map: models_1.Pullrequest.getAttributeTypeMap(),
        default: [
            'id',
            'title',
            'state',
            'source',
            'destination',
            'author',
            'comment_count',
            'task_count',
            'created_on',
            'updated_on',
            'summary'
        ]
    },
    PullrequestAllOfLinks: {
        map: models_1.PullrequestAllOfLinks.getAttributeTypeMap(),
        default: []
    },
    PullrequestEndpoint: {
        map: models_1.PullrequestEndpoint.getAttributeTypeMap(),
        default: ['repository', 'commit', 'branch']
    },
    PullrequestEndpointBranch: {
        map: models_1.PullrequestEndpointBranch.getAttributeTypeMap(),
        default: ['default_merge_strategy', 'name', 'merge_strategies']
    },
    PullrequestEndpointCommit: {
        map: models_1.PullrequestEndpointCommit.getAttributeTypeMap(),
        default: ['commit']
    },
    Repository: {
        map: models_1.Repository.getAttributeTypeMap(),
        default: ['is_private', 'fork_policy', 'scm', 'language', 'description']
    },
    RepositoryAllOfLinks: {
        map: models_1.RepositoryAllOfLinks.getAttributeTypeMap(),
        default: []
    },
    Update: {
        map: [
            {
                name: 'description',
                baseName: 'description',
                type: 'string'
            },
            {
                name: 'title',
                baseName: 'title',
                type: 'string'
            },
            {
                name: 'destination',
                baseName: 'destination',
                type: 'PullrequestEndpoint'
            },
            {
                name: 'reason',
                baseName: 'reason',
                type: 'string'
            },
            {
                name: 'source',
                baseName: 'source',
                type: 'PullrequestEndpoint'
            },
            {
                name: 'state',
                baseName: 'state',
                type: 'Pullrequest.StateEnum'
            },
            {
                name: 'author',
                baseName: 'author',
                type: 'Account'
            },
            {
                name: 'date',
                baseName: 'date',
                type: 'Date'
            }
        ],
        default: ['date', 'title', 'description', 'author', 'source', 'destination', 'state', 'reason']
    },
    User: {
        map: models_1.User.getAttributeTypeMap(),
        default: ['display_name', 'website', 'has_2fa_enabled', 'account_status', 'is_staff']
    }
};
exports.default = parsers;
