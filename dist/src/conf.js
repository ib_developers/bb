"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const rc_1 = __importDefault(require("rc"));
const conf = rc_1.default('bb');
exports.default = conf;
