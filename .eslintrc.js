module.exports = {
  env: {
    es6: true,
    node: true,
    'jest/globals': true
  },
  extends: [
    'airbnb-base',
    'plugin:prettier/recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  plugins: ['jest'],
  settings: {
    "import/resolver": {
      node: {
        extensions: ['.ts']
      }
    }
  },
  rules: {
    '@typescript-eslint/indent': 'off',
  }
};
